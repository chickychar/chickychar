<?php
session_start();
@ob_start();
include "admin_includes/config.php";
include "admin_includes/common_functions.php";
?>
<!DOCTYPE html>
<html  class="no-js">
<head>
    <?php include_once'metahead.php';?>
</head>
<body>
    <header id="header">
        <?php include_once'header.php';?>
    </header>
    <!-- start banner Area -->
    <section class="about-banner relative">
        <div class="overlay overlay-bg"></div>
        <div class="container">
            <div class="row d-flex align-items-center justify-content-center">
                <div class="about-content col-lg-12">
                    <h1 class="text-white">
                        Menus
                    </h1>
                    <p class="text-white link-nav"><a href="index.html">Home </a> <span class="lnr lnr-arrow-right"></span> <a href="menu.php"> Menus</a></p>
                </div>
            </div>
        </div>
    </section>
    <!-- End banner Area -->

    <!-- Start menu-area Area -->
    <div class="clearfix"></div>
    <section class="menu-area section-gapp" id="menu">
        <div class="container">
            <!--  <ul class="filter-wrap filters col-lg-12 no-padding">
                        <li class="active" data-filter="*">All Menu</li>
                        <li data-filter=".breakfast">Breakfast<br>7am to 10:30am</li>
                        <li data-filter=".burgers">Gourmet Burgers</li>
                        <li data-filter=".chicken">Charchar Chickens</li>
                        <li data-filter=".beverages">Beverages</li>
                        
                    </ul>-->
            <div class="category side-top">
               
            <div class="row">     
<div class="col-md-12">    

<div class="alert-message"> </div>
</div>
</div> 
                    <div id="mainMenu" class="main-menuu category-menu">
                        <div class="menu-drop">
                            <form action="/action_page.php">
                                <div class="form-group">
                                    <select class="form-control catId" id="sel1" name="sellist1">
                                        <?php
                                            echo get_category();
                                        ?>
                                    </select>
                                </div>
                            </form>
                        </div>
                        <ul id="autoNav" class="main-nav subCats">
                            <?php
                            $sql_query = $conn->query("SELECT id,category_name,category_image FROM categories where status='0' ORDER BY category_name ASC limit 0,1");
                            $row_cat = $sql_query->fetch_assoc();
                            $mainCatId = $row_cat['id'];
                            $sqlsubcat_query = $conn->query("SELECT id,cat_id,sub_category_name FROM sub_categories where status='0' AND cat_id='$mainCatId' ORDER BY sub_category_name ASC LIMIT 0,4");
                            while ($rowSubcat =$sqlsubcat_query->fetch_assoc()) {
                            ?>

                            <li>
                                <a href="#section<?php echo $rowSubcat['id'];?>"><?php echo $rowSubcat['sub_category_name'];?></a>
                            </li>
                           <?php
                            }
                           ?>
                           <?php $sql = getData('sub_categories','cat_id',$mainCatId);
                           $catCount = $sql->num_rows;
                           $count1 = $catCount-4;
                           $sqlsubcat_query = $conn->query("SELECT id,cat_id,sub_category_name FROM sub_categories where status='0' AND cat_id='$mainCatId' ORDER BY sub_category_name ASC LIMIT 4,$count1");?>
                           <li id="autoNavMore" class="auto-nav-more">
                               <a href="#" class="more-btn">more</a>
                               <ul id="autoNavMoreList" class="auto-nav-more-list">
                                   <?php while ($rowSubcat =$sqlsubcat_query->fetch_assoc()) { ?>
                                   <li>
                                   <a href="#section<?php echo $rowSubcat['id'];?>"><?php echo $rowSubcat['sub_category_name'];?></a>
                                   </li>
                                   <?php } ?>
                               </ul>
                           </li>
                        </ul>
                    </div>
                    <div class="clear"></div>
                  
            </div>
        </div>
      
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-12 products">
                <?php
                $sql_query = $conn->query("SELECT id,category_name,category_image FROM categories where status='0' ORDER BY category_name ASC limit 0,1");
                $row_cat = $sql_query->fetch_assoc();
                $mainCatId = $row_cat['id'];
                $sqlsubcat_query = $conn->query("SELECT id,cat_id,sub_category_name FROM sub_categories where status='0' AND cat_id='$mainCatId' ORDER BY sub_category_name ASC");
                while ($rowSubcat =$sqlsubcat_query->fetch_assoc()) {
                ?>
                    <div class="food-menu" id="section<?php echo $rowSubcat['id'];?>">
                        <h2 class="price mt-5 mb-2"><?php echo $rowSubcat['sub_category_name'];?></h2>
                        <div class="row">
                            <?php $products = getData('products','subcat_id',$rowSubcat['id']);
                            while ($getProducts =$products->fetch_assoc()) { ?>
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
                                <input type="hidden" class="category_id<?php echo $getProducts['id'];?>" value="<?php echo $getProducts['cat_id'];?>" >
                                <input type="hidden" class="product_name<?php echo $getProducts['id'];?>" value="<?php echo $getProducts['product_name'];?>" >
                                <input type="hidden" class="product_price<?php echo $getProducts['id'];?>" value="<?php echo $getProducts['product_price'];?>" >
                                <input type="hidden" class="cartaddon_price<?php echo $getProducts['id'];?>" value="0" >
                                <div class="single-menu launch-modal" data-toggle="modal" data-target="#myModal<?php echo $getProducts['id'];?>">
                                    <div class="image">
                                        <img class="img-fluid" src="<?php echo $base_url;?>uploads/product_images/<?php echo $getProducts['product_image'];?>">
                                    </div>
                                    <div class="title-wrap d-flex justify-content-between pt-3">
                                        <h5><?php echo $getProducts['product_name'];?></h5>
                                        <h5 class="price">$<?php echo $getProducts['product_price'];?></h5>
                                    </div>
                                    <p><?php echo $getProducts['product_description'];?></p>
                                    <button class="cart-btn primary">Add To Cart</button>
                                </div>
                                <div class="modal fade menu-modal" id="myModal<?php echo $getProducts['id'];?>">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">

                                            <!-- Modal Header -->
                                            <div class="modal-header">
                                                <h4 class="modal-title">Add Extra Toppings</h4>
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>

                                            <!-- Modal body -->
                                            <div class="modal-body modal-menu">
                                                <div class="row">
                                                    <div class="col-xl-6">
                                                        <div class="image">
                                                            <img class="img-fluid" src="<?php echo $base_url;?>uploads/product_images/<?php echo $getProducts['product_image'];?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-6">
                                                        <div class="sour-dough">
                                                            <h1><?php echo $getProducts['product_name'];?></h1>
                                                            <p><?php echo $getProducts['product_description'];?></p>
                                                            <?php $addons = getData('product_details','product_code',$getProducts['product_code']);
                                                            while ($getaddons =$addons->fetch_assoc()) { 
                                                            $addonName = getSingleColumnName($getaddons['ptoduct_type_id'],'product_type_code','product_type','product_types');
                                                            ?>
                                                            <h4 class="pb-3"><?php echo $addonName;?>
                                                            <?php if($getaddons['required_id'] == 0) { ?>
                                                                <span>Required</span>
                                                            <?php } ?>
                                                            </h4>
                                                            <form>
                                                            <?php $types = getData('product_name_details','product_type_code',$getaddons['product_type_code']);
                                                            while ($gettypes =$types->fetch_assoc()) { 
                                                            $typeName = getSingleColumnName($gettypes['type_name_id'],'id','type_name','product_type_details');
                                                            $typePrice = getSingleColumnName($gettypes['type_name_id'],'id','type_price','product_type_details');
                                                            ?>
                                                                <div class="custom-control custom-checkbox mb-3">
                                                                    <input type="checkbox" class="custom-control-input add_on" product-id="<?php echo $getProducts['id'];?>" addon-id="<?php echo $gettypes['id']; ?>" id="customCheck<?php echo $gettypes['id']; ?>" name="example1" value="<?php echo $gettypes['id']; ?>">
                                                                    <label class="custom-control-label" for="customCheck<?php echo $gettypes['id']; ?>"><?php echo $typeName;?></label>
                                                                    <?php if($typePrice != 0) { ?>
                                                                    <label class="custom-control-label custom-label float-right" for="customCheck">+ $<?php echo $typePrice;?>
                                                                    <?php } ?>
                                                                    <input type="hidden" class="addon_price<?php echo $gettypes['id']; ?>" value="<?php echo $typePrice;?>">
                                                                   </label>
                                                                </div>
                                                            <?php } ?>
                                                            </form>
                                                            <?php } ?>
                                                            <h4 class="pb-3">Special Instructions</h4>
                                                            <div class="form-group">
                                                                <input type="text" class="form-control m-form product_note<?php echo $getProducts['id'];?>" id="usr" placeholder="Add note extra sauce,no onions etc...">
                                                            </div>

                                                        </div>
                                                        <div class="container">
                                                            <div class="row">
                                                                <div class="col-xl-6 col-md-6 col-6 pt-2">
                                                                    <div class="qty">
                                                                       <span class="minus bg-dark">-</span>
                                                                       <input type="number" class="count product_quantity<?php echo $getProducts['id'];?>" name="qty" value="1">
                                                                       <span class="plus bg-dark">+</span>
                                                                   </div>
                                                                </div>
                                                                
                                                                <div class="col-xl-6 col-md-6 col-6 cart-btn menu-btn primary save_cart" id="<?php echo $getProducts['id'];?>">
                                                                    <span class="float-left btn-alert"  id="alert1">ADD ITEM</span><span class="float-right cart_price<?php echo $getProducts['id'];?>"> Total ₹ <?php echo $getProducts['product_price'];?></span>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <?php } ?>
                </div>
                <div class="col-lg-4 col-md-12 ">
                    <div class="checkout mt-5 rounded">
                      
                        <div class="row">
                            <div class="col-lg-6 col-md-5 col-4">Palak Paneer</div>
                            <div class=" col-lg-3 col-md-4 col-4"><button type="button" class="btn btn-light add-btn">- 1 +</button></div>
                            <div class="col-lg-3 col-md-3 col-4"><span>$25.00</span></div>

                        </div>
                        <ul class="mt-3">
                            <li>Subtotal<span>$50.00</span></li>


                        </ul>
                        <div class="row mt-3">
                          <a class="col-4 chekout-btn primary mb-3 text-center" href="cart.php">Checkout</a>
                            </div>
                    </div>
                </div>

            </div>

        </div>
    </section>
    <!-- End menu-area Area -->

    <!-- Start reservation Area -->
    <section class="reservation-area section-gap relative">
        <div class="overlay overlay-bg"></div>
        <div class="container">
            <div class="row justify-content-between align-items-center">
                <div class="col-lg-6 reservation-left">
                    <h1 class="text-white">If you’re organising a function, an all day workshop or even a party at home,</h1>
                    <p class="text-white pt-20">
                        We would be happy to help and offer our suggestions.
                    </p>


                </div>
                <div class="col-lg-5 reservation-right">
                    <form class="form-wrap text-center" action="catering-mail.php" method="post">
                        <input type="text" class="form-control" name="name" placeholder="Your Name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Your Name'">
                        <input type="email" class="form-control" name="email" placeholder="Your Email Address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Your Email Address'">
                        <input type="text" class="form-control" name="phone" placeholder="Phone Number" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Phone Number'">

                        <textarea class="form-control" name="additional-info" placeholder="Additional Information" required></textarea>
                        <button class="primary-btn text-uppercase mt-20">Send Enquiry</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- End reservation Area -->

    <footer class="footer-area">
        <?php include_once'footer.php';?>
    </footer>

 <script type="text/javascript">
    $(document).ready(function(){
       
        $('.launch-modal').click(function(){
           
            $('#myModal').modal({
                alert(1);
                backdrop: 'static'
            });
        });
    });
    </script>
    <script>
        $(document).ready(function() {
            // Add scrollspy to <body>
            $('.category-menu').scrollspy({
                target: ".category-menu",
                offset: 50
            });

            // Add smooth scrolling on all links inside the navbar
            $("#autoNav a").on('click', function(event) {
                // Make sure this.hash has a value before overriding default behavior
                if (this.hash !== "") {
                    // Prevent default anchor click behavior
                    event.preventDefault();

                    // Store hash
                    var hash = this.hash;

                    // Using jQuery's animate() method to add smooth page scroll
                    // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
                    $('html, body').animate({
                        scrollTop: $(hash).offset().top
                    }, 900, function() {

                        // Add hash (#) to URL when done scrolling (default click behavior)
                        window.location.hash = hash;
                    });
                } // End if
            });
        });

    </script>

    <script>
        // MAIN MENU
        const $mainMenu = $("#mainMenu");
        const $autoNav = $("#autoNav");
        const $autoNavMore = $("#autoNavMore");
        const $autoNavMoreList = $("#autoNavMoreList");
        autoNavMore = () => {
            let childNumber = 2;

            if ($(window).width() >= 320) {
                // GET MENU AND NAV WIDTH
                const $menuWidth = $mainMenu.width();
                const $autoNavWidth = $autoNav.width();
                if ($autoNavWidth > $menuWidth) {
                    // CODE FIRES WHEN WINDOW SIZE GOES DOWN
                    $autoNav
                        .children(`li:nth-last-child(${childNumber})`)
                        .prependTo($autoNavMoreList);
                    autoNavMore();
                } else {
                    // CODE FIRES WHEN WINDOW SIZE GOES UP
                    const $autoNavMoreFirst = $autoNavMoreList
                        .children("li:first-child")
                        .width();
                    // CHECK IF ITEM HAS ENOUGH SPACE TO PLACE IN MENU
                    if ($autoNavWidth + $autoNavMoreFirst < $menuWidth) {
                        $autoNavMoreList.children("li:first-child").insertBefore($autoNavMore);
                    }
                }
                if ($autoNavMoreList.children().length > 0) {
                    $autoNavMore.show();
                    childNumber = 2;
                } else {
                    $autoNavMore.hide();
                    childNumber = 1;
                }
            }
        };
        // INIT
        autoNavMore();
        $(window).resize(autoNavMore);
        // MAIN MENU END

    </script>
    <script>
window.onscroll = function() {myFunction()};

var navbar = document.getElementById("mainMenu");
var sticky = navbar.offsetTop;

function myFunction() {
  if (window.pageYOffset >= sticky) {
    navbar.classList.add("stickyy")
  } else {
    navbar.classList.remove("stickyy");
  }
}
</script>
<script>
      $(document).ready(function() {
           $('.count').prop('disabled', true);
           $(document).on('click', '.plus', function() {
               $('.count').val(parseInt($('.count').val()) + 1);
           });
           $(document).on('click', '.minus', function() {
               $('.count').val(parseInt($('.count').val()) - 1);
               if ($('.count').val() == 0) {
                   $('.count').val(1);
               }
           });
       });
  </script>
 
</body>

</html>
