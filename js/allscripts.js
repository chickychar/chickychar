//For User Registration
   $(".user_regBtn").on('click',function(){
        var user_name = $(".uname1").val();
        var user_email = $(".uemail1").val();
        var user_mobile = $(".umobile1").val();
        var user_password = $(".upassword1").val();
       /* var emailReg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;*/
    if(user_name == "" ){
    $(".errorList2").text("Name should not be empty");
    return false;
    }
    if(user_mobile == "" ){
      $(".errorList2").text("mobile should not be empty");
      return false;
    } 
    if(user_email == "" ){
      $(".errorList2").text("Email should not be empty");
      return false;
    }   
    /*if(!emailReg.test(email)){
      $(".errorList2").text("Enter valid email");
      return false;
    } */   
    if(user_password == "" ){
      $(".errorList2").text("Password should not  be empty");
      return false;
    }   
      $.ajax({
      url: "regAjax.php",
      type: 'POST',
      data:{                
       user_mobile:user_mobile,
     },
      success: function(response) {
        if(response == 0){
          $(".uname2").val(user_name); 
          $(".uemail2").val(user_email);
          $(".umobile2").val(user_mobile);
          $(".upassword2").val(user_password);
           $(".form_otp").show(); 
          $(".form_reg").hide(); 
        }

      },
      error: function(jqXHR, textStatus, errorThrown){
        console.log(textStatus, errorThrown);
      }
    });
           
    });
    //For Otp
    $(".user_otpBtn").on('click',function(){
        var user_name = $(".uname2").val();
        var user_email = $(".uemail2").val(); 
        var user_mobile = $(".umobile2").val();
        var user_password = $(".upassword2").val();
        var mobile_otp = $(".ucode1").val();
        if(mobile_otp == "" ){
       $(".errorList3").text("Please enter Code");
       return false;
     }     
      $.ajax({
      url: "otpAjax.php",
      type: 'POST',
      data:{            
        user_name:user_name,user_email:user_email,user_mobile:user_mobile,user_password:user_password,mobile_otp:mobile_otp,
      },
      success: function(response) {
        if(response == 0){
          alert("OTP Verified Successfully")
          window.location.href = 'index.php';
        }
        else{
          $(".errorList3").text("Please enter Valid Code");
      return false;
        } 
      },
      error: function(jqXHR, textStatus, errorThrown){
        console.log(textStatus, errorThrown);
      }
    });  
    });

    $(".userLogin").on('click',function(){
        var user_email = $(".useremail").val();
        var user_password = $(".userpassword").val();
        var emailReg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if(user_email == "" ){
            $(".errorList").text("Email should not be empty");
            return false;
        }       
        if(!emailReg.test(user_email)){
            $(".errorList").text("Enter valid email");
            return false;
        }    
        if(user_password == "" ){
            $(".errorList").text("Password should not  be empty");
            return false;
        }  
      $.ajax({
      url: "loginAjax.php",
      type: 'POST',
      data:{            
        user_email:user_email,user_password:user_password,
      },
      success: function(response) {
        if(response == 0){
           alert("Login Successfully");
           window.location.href = 'index.php';
        }
        else{
          alert("Please Enter valid Credentials");
        }
      },
      error: function(jqXHR, textStatus, errorThrown){
        console.log(textStatus, errorThrown);
      }
    });
       
    });
    
$(".catId").change(function(){
    id =$(this).val();
    $.ajax({
      url: 'getCatAjax.php',
      type: 'POST',
      data:{            
        catId:id,
      },
      success: function(response) {
        $(".products").html(response);
      },
    });
    $.ajax({
        url: 'getsubCats.php',
        type: 'POST',
        data:{		        
            catId:id,
        },
        success: function(response) {
            alert(response);
            $(".subCats").html(response);
        },
    });
});
$('body').on('click', '.save_cart', function() {
    var productId = $(this).attr('id');
    var catId = $('.category_id'+productId).val();
    var productName = $('.product_name'+productId).val();
    var productPrice = $('.product_price'+productId).val();
    var product_quantity = $('.product_quantity'+productId).val();
    var product_note = $('.product_note'+productId).val();
    var cartaddon_price = $('.cartaddon_price'+productId).val();
    var addOn = [];
    $(".add_on:checked").each(function() {
    addOn.push(this.value);
    });
    if(addOn == '') {
        addOn.push(id='');
    }
    $.ajax({
        type:'post',
        url:'save_cart.php',
        data:{		        
            productId:productId,catId:catId,productName:productName,productPrice:productPrice,product_quantity:product_quantity,cartaddon_price:cartaddon_price,product_note:product_note,addOn:addOn,
        },
        success:function(response) {
            alert(response);
            if(response == 1) {
                alert("Products Added into Your Cart");
                window.location.href = "index.php";
            } else if(response == 0) {
                alert("Products Not Added into Your Cart");
                window.location.href = "index.php";
            }
        }
    }); 
});
$('body').on('click', '.save_cart1', function() {
    var productId = $(this).attr('id');
    var catId = $('.category_id'+productId).val();
    var productName = $('.product_name'+productId).val();
    var productPrice = $('.product_price'+productId).val();
    var product_quantity = $('.product_quantity'+productId).val();
    var product_note = $('.product_note'+productId).val();
    var cartaddon_price = $('.cartaddon_price'+productId).val();
    var addOn = [];
    $(".add_on:checked").each(function() {
    addOn.push(this.value);
    });
    if(addOn == '') {
        addOn.push(id='');
    }
    $.ajax({
        type:'post',
        url:'save_cart.php',
        data:{		        
            productId:productId,catId:catId,productName:productName,productPrice:productPrice,product_quantity:product_quantity,cartaddon_price:cartaddon_price,product_note:product_note,addOn:addOn,
        },
        success:function(response) {
            alert(response);
            if(response == 1) {
                alert("Products Added into Your Cart");
                window.location.href = "index.php";
            } else if(response == 0) {
                alert("Products Not Added into Your Cart");
                window.location.href = "index.php";
            }
        }
    }); 

});

$('body').on('click', '.add_on', function() {
    var addonId = $(this).attr('addon-id');
    var productId = $(this).attr('product-id');
    var price1 = $('.addon_price'+addonId).val();
    var price2 = $('.cartaddon_price'+productId).val();
    var productPrice = $('.product_price'+productId).val();
    if($(this).prop("checked") == true) {
        var price=parseFloat(price1)+parseFloat(price2); 
        var cart_price=parseFloat(productPrice)+parseFloat(price1); 
    } else if($(this).prop("checked") == false) {
        var price=parseFloat(price2)-parseFloat(price1);
        var cart_price=parseFloat(productPrice)-parseFloat(price1);
    }
    $('.cartaddon_price'+productId).val(price);
    $('.cart_price'+productId).text(" Total ₹ "+cart_price);
    $('.product_price'+productId).val(cart_price);
});

$('body').on('click', '.add_on1', function() {
    var addonId = $(this).attr('addon-id');
    var productId = $(this).attr('product-id');
    var price1 = $('.addon_price'+addonId).val();
    var price2 = $('.cartaddon_price'+productId).val();
    var productPrice = $('.product_price'+productId).val();
    if($(this).prop("checked") == true) {
        var price=parseFloat(price1)+parseFloat(price2); 
        var cart_price=parseFloat(productPrice)+parseFloat(price1); 
    } else if($(this).prop("checked") == false) {
        var price=parseFloat(price2)-parseFloat(price1);
        var cart_price=parseFloat(productPrice)-parseFloat(price1);
    }
    $('.cartaddon_price'+productId).val(price);
    $('.cart_price'+productId).text(" Total ₹ "+cart_price);
    $('.product_price'+productId).val(cart_price);
});