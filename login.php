<!DOCTYPE html>
<html lang="zxx" class="no-js">

<head>
    <?php include_once'metahead.php';?>
</head>

<body>
     <header id="header">
      <?php include_once'header.php';?>
    </header>

    <!-- start banner Area -->
    <section class="about-banner relative">
        <div class="overlay overlay-bg"></div>
        <div class="container">
            <div class="row d-flex align-items-center justify-content-center">
                <div class="about-content col-lg-12">
                    <h1 class="text-white">
                        Menus
                    </h1>
                    <p class="text-white link-nav"><a href="index.php">Home </a> <span class="lnr lnr-arrow-right"></span> <a href="menu.php">Menus</a></p>
                </div>
            </div>
        </div>
    </section>
    <!-- End banner Area -->

    <section class="section section-sm section-first bg-default text-md-left form_reg">
        <div class="container">
            <div class="row row-50 pt-5 justify-content-center">
                <div class="col-md-10 col-lg-6">
                    <h3 class="font-weight-medium">Login</h3>
                    <form class="form-checkout b-we mt-3" method="POST" action="#">
                           <span class="errorList"></span>
                        <div class="row row-30">
                            <div class="col-12">
                                <div class="form-wrap">
                                    <input class="form-input useremail" id="email1" type="text" name="user_email" data-constraints="@Required" placeholder="Enter Email">

                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-wrap has-error">

                                    <input class="form-input form-control-has-validation userpassword" id="pwrd1" type="password" name="user_password" data-constraints="@Required" placeholder="Password">
                                </div>
                               
                            </div>

                            <div class="col-12 pt-3">
                                <button type="button" class="login-btn primary mb-3 text-center userLogin">Login</button>
                                 <label class="checkbox-inline pt-3 float-right">
                                    <input name="input-checkbox-1" value="checkbox-1" type="checkbox" class="checkbox-custom"><span class="checkbox-custom-dummy"></span>Forget Password?
                                </label>
                            </div>


                        </div>


                    </form>
                </div>
                <div class="col-md-10 col-lg-6">
                    <h3 class="font-weight-medium">SignUp</h3>
                    <form class="form-checkout b-we mt-3 mb-2" method="POST" action="#">
                        <div class="row row-30">
                            <span class="errorList2"></span>
                            <div class="col-12">
                                <div class="form-wrap">
                                    <input class="form-input uname1" id="name" type="text" name="user_name" data-constraints="@Required" placeholder="Name">

                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-wrap">
                                    <input class="form-input uemail1" id="email" type="email" name="user_email" data-constraints="@Required" placeholder="Email">

                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-wrap">
                                    <input class="form-input umobile1" id="mobile" type="number" name="user_mobile" data-constraints="@Required" placeholder="Mobile">

                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-wrap has-error">

                                    <input class="form-input form-control-has-validation upassword1" id="pwrd" type="password" name="user_password" data-constraints="@Required" placeholder="Password">

                                </div>
                            </div>
                           <!--  <div class="col-12">
                                <div class="form-wrap has-error">

                                    <input class="form-input form-control-has-validation" id="cnfmpwrd" type="password" name="conform_password" data-constraints="@Required" placeholder="Confirm Password">

                                </div>
                            </div> -->
                            <div class="col-12 pt-3">
                                <button type="button" class="login-btn primary mb-3 text-center user_regBtn">Sign Up</button>
                            </div>

                        </div>

                    </form>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-sm section-first bg-default text-md-left form_otp" style='display:none'>
        <div class="container">
            <div class="row row-50 pt-5 justify-content-center mb-4">
                <div class="col-md-10 col-lg-6">
                    <h3 class="font-weight-medium">OTP</h3>
                    <form class="form-checkout b-we mt-3" method="POST" action="#">
                        <div class="row row-30">

                            <div class="col-12">
                                <div class="form-wrap">
                                     <span class="errorList3"></span>
                                    <input class="form-input ucode1" id="otp" type="text" name="mobile_otp" data-constraints="@Required" placeholder="OTP">
                                    <input class="uname2" type="hidden" name="user_name" value="<?php echo $_POST['user_name']; ?>">
                                    <input class="uemail2" type="hidden" name="user_email" value="<?php echo $_POST['user_email']; ?>">
                                    <input class="umobile2" type="hidden" name="user_mobile" value="<?php echo $_POST['user_mobile']; ?>">
                                    <input class="upassword2" type="hidden" name="user_password" value="<?php echo $_POST['user_password']; ?>">
                                </div>
                            </div>
                            <div class="col-12 pt-3">
                                <button type="button" class="login-btn primary mb-3 text-center user_otpBtn">Verify OTP</button>
                                
                            </div>


                        </div>


                    </form>
                </div>
              
            </div>
        </div>
    </section>
  <footer class="footer-area">
        <?php include_once'footer.php';?>
    </footer>

</body>

</html>
