<?php 
session_start();
@ob_start();
include "admin_includes/config.php";
include "admin_includes/common_functions.php";
$mainCatId = $_POST['catId'];
$sqlsubcat_query = $conn->query("SELECT id,cat_id,sub_category_name FROM sub_categories where status='0' AND cat_id='$mainCatId' ORDER BY sub_category_name ASC");
while ($rowSubcat =$sqlsubcat_query->fetch_assoc()) {
    $products = getData('products','subcat_id',$rowSubcat['id']);
echo '<div class="food-menu" id="section'.$rowSubcat['id'].'">
    <h2 class="price mt-5 mb-2">'.$rowSubcat['sub_category_name'].'</h2>
    <div class="row">';
        while ($getProducts =$products->fetch_assoc()) { 
        echo'<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
        <input type="hidden" class="category_id'.$getProducts['id'].'" value="'.$getProducts['cat_id'].'" >
        <input type="hidden" class="product_name'.$getProducts['id'].'" value="'.$getProducts['product_name'].'" >
        <input type="hidden" class="product_price'.$getProducts['id'].'" value="'.$getProducts['product_price'].'">
        <input type="hidden" class="cartaddon_price'.$getProducts['id'].'" value="0" >
            <div class="single-menu " data-toggle="modal" data-target="#myModal'.$getProducts['id'].'">
                <div class="image">
                    <img class="img-fluid" src="'.$base_url.'uploads/product_images/'.$getProducts['product_image'].'">
                </div>
                <div class="title-wrap d-flex justify-content-between pt-3">
                    <h5>'.$getProducts['product_name'].'</h5>
                    <h5 class="price">$ '.$getProducts['product_price'].'</h5>
                </div>
                <p>'.$getProducts['product_description'].'</p>
                <button class="cart-btn primary">Add To Cart</button>
            </div>
            <div class="modal fade menu-modal" id="myModal'.$getProducts['id'].'">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">

                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">Add Extra Toppings</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <!-- Modal body -->
                        <div class="modal-body modal-menu">
                            <div class="row">
                                <div class="col-xl-6">
                                    <div class="image">
                                        <img class="img-fluid" src="'.$base_url.'uploads/product_images/'.$getProducts['product_image'].'">
                                    </div>
                                </div>
                                <div class="col-xl-6">
                                    <div class="sour-dough">
                                        <h1>'.$getProducts['product_name'].'</h1>
                                        <p>'.$getProducts['product_description'].'</p>';
                                        $addons = getData('product_details','product_code',$getProducts['product_code']);
                                        while ($getaddons =$addons->fetch_assoc()) { 
                                        $addonName = getSingleColumnName($getaddons['ptoduct_type_id'],'product_type_code','product_type','product_types');
                                        echo'<h4 class="pb-3">'.$addonName.'';
                                        if($getaddons['required_id'] == 0) { 
                                            echo'<span>Required</span>';
                                        }
                                        echo'</h4><form>';
                                        $types = getData('product_name_details','product_type_code',$getaddons['product_type_code']);
                                        while ($gettypes =$types->fetch_assoc()) { 
                                        $typeName = getSingleColumnName($gettypes['type_name_id'],'id','type_name','product_type_details');
                                        $typePrice = getSingleColumnName($gettypes['type_name_id'],'id','type_price','product_type_details');
                                            echo'<div class="custom-control custom-checkbox mb-3">
                                                <input type="checkbox" class="custom-control-input add_on1"
                                                product-id="'.$getProducts['id'].'" addon-id="'.$gettypes['id'].'" id="customCheck'.$gettypes['id'].'" name="example1" value="'.$gettypes['id'].'">
                                                <label class="custom-control-label" for="customCheck'.$gettypes['id'].'">'.$typeName.'</label>';
                                                if($typePrice != 0) {
                                                echo'<label class="custom-control-label custom-label float-right" for="customCheck">+ $'.$typePrice.'';
                                                }
                                                echo'<input type="hidden" class="addon_price'.$gettypes['id'].'" value="'.$typePrice.'">
                                            </div>';
                                        }
                                        echo '</form>';
                                        }
                                        echo'<h4 class="pb-3">Special Instructions</h4>
                                        <div class="form-group">
                                            <input type="text" class="form-control m-form product_note'.$getProducts['id'].'" id="usr" placeholder="Add note extra sauce,no onions etc...">
                                        </div>

                                    </div>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-xl-6 col-md-6 col-6 quantity">
                                                <div class="qty">
                                                    <span class="minus bg-dark">-</span>
                                                    <input type="number" class="count product_quantity'.$getProducts['id'].'" name="qty" value="1">
                                                    <span class="plus bg-dark">+</span>
                                                </div>
                                            </div>
                                            <div class="col-xl-5 col-md-5 col-5 cart-btn menu-btn primary save_cart1" id="'.$getProducts['id'].'">
                                                <span class="float-left">ADD ITEM</span><span class="float-right cart_price'.$getProducts['id'].'"> Total ₹ '.$getProducts['product_price'].'</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>';
        }
    echo'</div>
</div>';
}
?>