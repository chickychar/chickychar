<?php
    function getDataFromTables($table=NULL,$status=NULL,$clause=NULL,$id=NULL,$activeStatus=NULL,$activeTop=NULL) {

        global $conn;
        if($table!='' && $table!=NULL && $clause!='' && $clause!=NULL && $id!='' && $id!=NULL) {
            //Get All Table Data with Where Condition(4)
            $sql="select * from `$table` WHERE `$clause` = '$id' ";
        } elseif($table!='' && $table!=NULL && $status!='' && $status!=NULL) {
            //Get Active Records (3)
            $sql="select * from `$table` WHERE `status` = '$status' ORDER BY id DESC";
        } elseif($table!='' && $table!=NULL && $activeTop!='' && $activeTop!=NULL) {
            //Get All Active records top Table Data (6)
            $sql="select * from `$table` ORDER BY status, id DESC ";
        } elseif($table!='' && $table!=NULL) {
            //Get All Table Data (1)
            $sql="select * from `$table` ORDER BY status, id DESC";
        }  else {
            //Last if fail then go to this
            $sql="select * from `$table` ORDER BY status, id DESC";
        }

        $result = $conn->query($sql);
        return $result;
    }
    function getAllDataWithStatus($table,$status)  {
        global $conn;
        $sql="select * from `$table` WHERE `status` = '$status' ";
        $result = $conn->query($sql); 
        return $result;
    }
    function getIndividualDetails($id,$table,$clause)
    {
        global $conn;
        $sql="select * from `$table` where `$clause` = '$id' ";
        $result = $conn->query($sql);
        $row = $result->fetch_assoc();        
        return $row;
    }
    function checkUserAvail($table,$clause,$value){
        global $conn;
        $sql = "SELECT * FROM `$table` WHERE `$clause`= '$value' ";
        $result = $conn->query($sql);
        return $result->num_rows;
    }
    /*Common function with where out where get all data from query */
    function getAllData($table)
    {
        global $conn;
        $sql="select * from `$table` ";
        $result = $conn->query($sql);         
        return $result;
    }
    
    /*Common function with where out where get all data from query */
    function getAllDataWithActiveRecent($table)  {
        global $conn;
        $sql="select * from `$table` ORDER BY status, id DESC ";
        $result = $conn->query($sql); 
        return $result;
    }

     /*Common function with where and check active status for get all records*/
    function getAllDataCheckActiveRecords($table,$status)
    {
        global $conn;
        $sql="select * from `$table` WHERE `status` = '$status' ORDER BY id DESC  ";
        $result = $conn->query($sql);         
        return $result;
    }

    /*Common function with where clause */
    function getAllDataWhere($table,$clause,$id)
    {
        global $conn;
      $sql="select * from `$table` WHERE `$clause` = '$id' "; 
        $result = $conn->query($sql);        
        return $result;
    }

    /* Common function for get count for rows */
     function getRowsCount($table)  {
        global $conn;
        $sql="select * from `$table` ";
        $result = $conn->query($sql);
        $noRows = $result->num_rows;
        return $noRows;
    }

    /* encrypt and decrypt password */
     function encryptPassword($pwd) {
        $key = "123";
        $admin_pwd = bin2hex(openssl_encrypt($pwd,'AES-128-CBC', $key));
        return $admin_pwd;
    }

    function decryptPassword($admin_password) {
        $key = "123";
        $admin_pwd = openssl_decrypt(hex2bin($admin_password),'AES-128-CBC',$key);
        return $admin_pwd;
    }

    function sendMobileOTP($user_otp,$user_mobile) {
        global $conn;
        $username = "vivek";
        $password = "12345";
        $numbers = "$user_mobile"; // mobile number
        $sender = urlencode('FDDOSE'); // assigned Sender_ID
        $message = urlencode('OTP from Chickychar is '.$user_otp.' . Do not share it with any one.'); // Message text required to deliver on mobile number
        //$data = "userid="."$username"."&password="."$password"."&mobno="."$numbers"."&msg="."$message"."&senderid="."$sender"&route=4";
        $data = "http://msgalert.in/api?userid=vivek&password=12345&mobno=".$numbers."&msg=".$message."&senderid=FDDOSE&route=4";
        $ch = curl_init();
        //curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch,CURLOPT_URL,$data);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        $response = curl_exec($ch); 


    }

    function sendMobileOrderPlaced($orderId,$user_mobile,$orderTotal) {
        global $conn;
        $username = "vivek";
        $password = "12345";
        $numbers = "$user_mobile"; // mobile number
        $sender = urlencode('FDDOSE'); // assigned Sender_ID
        $message = urlencode('Thank You for Ordering FOODDOSE. Your Order Number is: '.$orderId.' Order Total: '.$orderTotal.' '); // Message text required to deliver on mobile number
        //$data = "userid="."$username"."&password="."$password"."&mobno="."$numbers"."&msg="."$message"."&senderid="."$sender"&route=4";
        $data = "http://msgalert.in/api?userid=vivek&password=12345&mobno=".$numbers."&msg=".$message."&senderid=FDDOSE&route=4";
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,$data);
        $response = curl_exec($ch); 
    }

function sendMobileOrderCancelled($orderId,$user_mobile) {
        global $conn;
        $username = "vivek";
        $password = "12345";
        $numbers = "$user_mobile"; // mobile number
        $sender = urlencode('FDDOSE'); // assigned Sender_ID
        $message = urlencode('Your order is cancelled. Apologies for any inconvenience. Your Order Number is: '.$orderId.' '); // Message text required to deliver on mobile number
        //$data = "userid="."$username"."&password="."$password"."&mobno="."$numbers"."&msg="."$message"."&senderid="."$sender"&route=4";
        $data = "http://msgalert.in/api?userid=vivek&password=12345&mobno=".$numbers."&msg=".$message."&senderid=FDDOSE&route=4";
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,$data);
        $response = curl_exec($ch); 
    }

    function getImageUnlink($val,$table,$clause,$id,$target_dir) {
        global $conn;
        $getBanner = "SELECT $val FROM $table WHERE $clause='$id' ";
        $getRes = $conn->query($getBanner);
        $row = $getRes->fetch_assoc();
        $img = $row[$val];
        $path = $target_dir.$img.'';
        chown($path, 666);
        if (unlink($path)) {
            return 1;
        } else {
            return 0;
        }
    }
    function getColumnData($sql){
       global $conn;        
       $result = $conn->query($sql);
       $row = $result->fetch_assoc();        
       return $row;
   }
    function getCategory($id){
        $data = "select category_name from `categories` where id='$id'";
        /*echo $data; die;*/
        $dataVal = getColumnData($data);
        return $dataVal['category_name'];
    }
    function getSubCategory($id){
        $data = "select sub_category_name from `sub_categories` where id='$id'";
        /*echo $data; die;*/
        $dataVal = getColumnData($data);
        return $dataVal['sub_category_name'];
    }
    function getSingleColumnName($value,$column,$expColumn,$table){
        global $conn;
        $sql="select $expColumn from `$table` WHERE $column = '$value'";
        $result = $conn->query($sql); 
        $row = $result->fetch_assoc();
        if($row){
            return $row[$expColumn];
        }
        else{
            return "" ;
        }
    }
     function getSingleColumnValue($table,$expect_column,$column,$id){
         $data = "select $expect_column  from $table where $column='$id'";
        /*echo $data; die;*/
        $dataVal = getColumnData($data);
        return $dataVal[$expect_column];
    }
    function mobileCValidation($number){
        $mob="/^[1-9][0-9]*$/";
        if(!preg_match($mob,$number) || strlen($number)!='10'){
            return 1 ;
        }else{
            return 0 ;
        }
    }
    function pushNotification_new($userMobiletokendata,$title,$msg,$img=null,$status){
        define('API_ACCESS_KEY','AAAAjs3_yBs:APA91bHEhsaDRLwWWzNtxlsDpcookTFEx0mL22HzQMxeI3qTV6lgUdSA7TwXITMWP9bxJ1-4LeW9X0GOzrbfe1KC4QUQ2E30mwpCPnABB5VEjjqYTiSbrBKuw5YjdqsFy_JNUaL1RH8K' );
                $registrationIds =$userMobiletokendata;
                $countArr = count($registrationIds);
                if($countArr > 0)
                {
                    for($i=0; $i<$countArr ; $i++){                    
                        if($img == '' || $img == null){
                            $img ='0';
                        }
                        // prep the bundle
                        $msg = array
                        (
                            'message'     => $msg,
                            'title'        => $title,
                            'img'           => $img,  
                            'status_code'       => $status
                        );
                        
                        $fields = array
                        (
                            'registration_ids' => array($registrationIds[$i]),
                            'data'            => $msg
                        );
        
                        $headers = array
                        (
                            'Authorization: key=' . API_ACCESS_KEY,
                            'Content-Type: application/json'
                        );
                        $ch = curl_init();
                        curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
                        curl_setopt( $ch,CURLOPT_POST, true );
                        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );                               
                        $result = curl_exec($ch );                               
                        curl_close( $ch );
                        //echo $result ;
                    }
                }
    }
    function isAdmin()
{
    if(!isset($_SESSION['superuser_id']) || (trim($_SESSION['superuser_id']) == '')) 
    {
    header("location: index.php");
    exit();
    }
}
function getSingleRow($table,$id,$flag)
{
    $sql=  queryExecute("SELECT * FROM $table where $flag='0' order by $id DESC limit 0,1");
    return $sql;
}
function currentDateTime()
{
    $timezone = "Asia/Calcutta";
    date_default_timezone_set($timezone);
    $added = date("Y-m-d H:i:s");
    return $added;
}      
function dbRowUpdate($table_name, $form_data,$where_clause)
{
    // check for optional where clause
    $whereSQL = '';
    if(!empty($where_clause))
    {
        // check to see if the 'where' keyword exists
        if(substr(strtoupper(trim($where_clause)), 0, 5) != 'WHERE')
        {
            // not found, add key word
            $whereSQL = " WHERE ".$where_clause;
        }
        else
        {
            $whereSQL = " ".trim($where_clause);
        }
    }
    // start the actual SQL statement
    $sql = "UPDATE ".$table_name." SET ";

    // loop and build the column /
    $sets = array();
    foreach($form_data as $column => $value)
    {
        $sets[] = "`".$column."` = '".$value."'";
    }
    $sql .= implode(', ', $sets);

    // append the where statement
    $sql .= $whereSQL;

    // run and return the query result
    return queryExecute($sql);
}   
           
function dbRowInsert($table_name,$form_data)
{
   // retrieve the keys of the array (column titles)
    $fields = array_keys($form_data);
    // build the query
    $sql = "INSERT INTO ".$table_name."
    (`".implode('`,`', $fields)."`)
    VALUES('".implode("','", $form_data)."')";
    //run and return the query result resource
    return queryExecute($sql);
}
// developed by winspect_sn
function queryExecute($sql)
{
    global $conn;
    //example  mysql_query ;
    $res=$conn->query($sql); ;
    return $res;
}

function fetchArray($sql)
{
     global $conn;
    //example  mysql_fetch_array($result) ;
    $res1=mysql_fetch_array($sql);
    return $res1;
}  
function getSingleRecord($table,$critfield,$criteria,$ord=null) 
{      
    global $conn;
    //example  select * from member where id=2 ;
    if($ord==null){
    $sql = "SELECT * FROM $table WHERE $critfield='".$criteria."'";
    }else{
 $sql = "SELECT * FROM $table WHERE $critfield='".$criteria."'  order by id asc";
   }
    $res=queryExecute($sql);
    if($res->num_rows>0)
    {

        $row = $res->fetch_assoc();        
        return $row;    
    }   
}

function fetchAssoc($sql)
{
   //example  mysql_fetch_assoc($result);    
   $res2=  mysql_fetch_assoc($sql);
   return $res2;
}

function rowCount($tblcluse)
{
    //example  select count(*)  as count from members ;
    $res3=queryExecute("select count(*)  as count from $tblcluse");
    $sql_assoc=$res3->fetch_assoc();
    return $sql_assoc['count'];
}

function getUserUniqueId($base,$table,$column) 
{
    $num = 10000 ;      
    $timeStampData=microtime();
    list($msec, $sec)=explode(' ', $timeStampData);
    $msec =round($msec * 1000);
    $asdw=$sec.$msec;
    $userid = $base .$num.$asdw;
    while(1) 
    {                            
        $user = getSingleRecord("$table","$column",$userid);
        if ($user)
        {
            $num++;
           
            $userid =$base .$num.$asdw;
        }
        else 
        {   
            return $userid;
            break;
        }                       
    }
} 
function convertDate($date)
{
    $date=date_create($date);
    $datedata= date_format($date,"Y-m-d");
    return $datedata ;
}
//for Category
function get_category()
{
    $result = queryExecute("SELECT id,category_name,category_image FROM categories where status='0' ORDER BY category_name ASC");
    while($row = $result->fetch_assoc())
    {
    echo '<option value="'.$row["id"].'">'.$row['category_name'].'</option> ';
    }   
}    
function get_categoryname($id)
{
    $result = queryExecute("SELECT * FROM category where id = '$id'");
    $row = $result->fetch_assoc();
    return $row['cat_name'];
} 
function get_subcategory($flag)
{
    echo '<option value="">select subcategory </option> ';
    $result = queryExecute("SELECT * FROM subcategory where $flag='0'");
    while($row = $result->fetch_assoc())
    {
    echo '<option value="'.$row["id"].'">'.$row["subcategory"].'</option> ';
    }   
}

function getData($table,$clause,$id)
{
    global $conn;
    $sql="select * from `$table` WHERE `$clause` = '$id' AND status = 0 ";
    $result = $conn->query($sql);        
    return $result;
}

function ak_img_resize($target, $newcopy, $w, $h, $ext) {
    list($w_orig, $h_orig) = getimagesize($target);
    $scale_ratio = $w_orig / $h_orig;
    if (($w / $h) > $scale_ratio) {
           $w = $h * $scale_ratio;                                                                                                                                                       
    } else {
           $h = $w / $scale_ratio;
    }
    $img = "";
    $ext = strtolower($ext);
    if ($ext == "gif"){ 
      $img = imagecreatefromgif($target);
    } else if($ext =="png"){ 
      $img = imagecreatefrompng($target);
    } else { 
      $img = imagecreatefromjpeg($target);
    }
    $tci = imagecreatetruecolor($w, $h);
    // imagecopyresampled(dst_img, src_img, dst_x, dst_y, src_x, src_y, dst_w, dst_h, src_w, src_h)
    imagecopyresampled($tci, $img, 0, 0, 0, 0, $w, $h, $w_orig, $h_orig);
    imagejpeg($tci, $newcopy, 80);
}
function sendEmail($to,$subject,$message,$from,$name) {
    global $conn;   
    // Always set content-type when sending HTML email
    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";  
    $headers .= 'From: '.$name.'<'.$from.'>'. "\r\n";
    if(mail($to, $subject, $message, $headers)) {
        return 0;
    } else {
        return 1;
    }

}
function getImagename($id,$table,$column,$expcolumn)
{
    global $conn;
    $result = queryExecute("SELECT $expcolumn FROM $table where $column = '$id'");
    $row = $result->fetch_assoc();
    return $row[$expcolumn];
}
function getProductname($id)
{
    $result = queryExecute("SELECT product_name FROM product where productcode='$id'");
    $row = $result->fetch_assoc();
    return $row['product_name'];
}
function getImagePrddtlname($id)
{
    $result = queryExecute("SELECT image FROM product_detail where prddtl_id = '$id'");
    $row = $result->fetch_assoc();
    return $row['image'];
}
function dateConvert($date){
    
    $date1=date_create($date);
    return date_format($date1,"d-m-Y");
}
function dateConvertSecond($date){
    
    $date1=date_create($date);
    return date_format($date1,"d-M-Y");
}
function password_crypt( $string, $action = 'e' ) {
    // you may change these values to your own
    $secret_key = 'infysmart_key';
    $secret_iv = 'infysmart_iv';
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
    return $output;
    //$encrypted = password_crypt( 'Hello World!', 'e' );
    //$decrypted = password_crypt( 'RTlOMytOZStXdjdHbDZtamNDWFpGdz09', 'd' );
} 
function login_user($username,$password){
        global $conn; 
        $table = 'user_table';
        $myusername = mysqli_real_escape_string($conn,$username);
        $mypassword = mysqli_real_escape_string($conn,$password);
        $numrows=rowCount(" $table where `username`='$myusername' and `password`='$mypassword' AND status='0'");
        if($numrows>0)
        {
            return 1 ;
        }
        else 
        {
            return 0;
        }       
} 

//For User Login  -- DV
function userLogin($user_email,$user_password) {
        global $conn;
        $sql="SELECT * FROM users WHERE (user_email = '$user_email' OR user_mobile = '$user_email') AND user_password = '$user_password' AND status = 0";
        $result = $conn->query($sql);        
        return $result;
    }   
    
define("MSG_FIELD_MISSING", "Required field(s) is missing", true);
define("MSG_INVALID_REQUEST", "Invalid request", true);
define("MSG_SUCCESS", "Success", true);
define("MSG_NORECORDS", "No records Found", true);
define("MSG_RECORDEXIST", "Record Exist", true);
define("MSG_INVCALIDMOBILE", "Invalid Mobile Number", true);
define("MSG_OTP_SUCCESS", "Otp Send Successfully", true);
define("MSG_INVALID_OTP", "Invalid otp", true);
define("MSG_INVALID_ERROR", "opps some error", true);
define("MSG_RECORDEXISTUSER", "Please enter valid credentails!", true);
define("MSG_NOTRECORDEXISTUSER", "User Not Register", true);
define("MSG_INVALIDCREDENTIALS", "User Not Register", true);
define("RESCODE_SUCCESS", "0", true);
define("RESCODE_NORECORDS", "1", true);
define("RESCODE_PARAMETERMISSING", "2", true);
define("RESCODE_EXIST", "3", true);
define("RESCODE_ERROR", "4", true);
define("RESCODE_INVALID", "5", true);
define("RESCODE_OTP_SUCCESS", "6", true);
define("RESCODE_OTP_FAIL", "7", true);
define("RESCODE_NOTRECORDEXISTUSER", "8", true);
?>
