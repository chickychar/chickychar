<!DOCTYPE html>
<html lang="zxx" class="no-js">

<head>
    <?php include_once'metahead.php';?>
</head>

<body>
    <header id="header">
        <?php include_once'header.php';?>
    </header><!-- #header -->

    <!-- start banner Area -->
    <section class="about-banner relative">
        <div class="overlay overlay-bg"></div>
        <div class="container">
            <div class="row d-flex align-items-center justify-content-center">
                <div class="about-content col-lg-12">
                    <h1 class="text-white">
                        About Us
                    </h1>
                    <p class="text-white link-nav"><a href="index.html">Home </a> <span class="lnr lnr-arrow-right"></span> <a href="about.html"> About Us</a></p>
                </div>
            </div>
        </div>
    </section>
    <!-- End banner Area -->

    <!-- Start home-about Area -->
    <section class="home-about-area section-gap">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 home-about-left">
                    <h1>ChickyCharChar! Story</h1>
                    <p>
                        ChickyCharChar launched in 2015, opening our first store in Alexandria on the 16th August. The owners have been in hospitality for over 30 years, operating successful food businesses across the Sydney CBD and suburbs.
                    </p>

                </div>
                <div class="col-lg-6 home-about-right">
                    <img class="img-fluid" src="img/about-img2.jpg" alt="">
                </div>
            </div>
        </div>
    </section>
    <!-- End home-about Area -->

    <!-- Start services Area -->
    <section class="services-area pb-120">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="menu-content pb-70 col-lg-8">
                    <div class="title text-center">
                        <h1 class="mb-10">What Kind of Services we Offer</h1>
                        <p>“ChickyCharChar is about delivering a great experience for all ages with food that is fresh, tasty and wholesome.”</p>
                    </div>
                </div>
                <div class="container">
                    <p class="sample-text">
                        In today’s world, everyone is busy and very conscious of their diet. We pride ourselves on the quality of our food and most importantly you can be assured that we use only the freshest ingredients, food that is free of chemicals and hormones and our cooking methods are synonymous with healthiest of healthy eating. So, whether you are looking for a great lunch, a simple yet excellent casual dinner or even a cheeky snack, be assured you will find it at ChickyCharChar.

                    </p>
                    <br><br>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <div class="single-service">
                        <div class="thumb">
                            <img src="img/about-img.png" alt="">
                        </div>
                        <a href="#">
                            <h4>Flame Grilled BBQ Chicken</h4>
                        </a>
                        <!---<p>
									inappropriate behavior is often laughed off as “boys will be boys,” women face higher conduct women face higher conduct.
								</p>!-->
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="single-service">
                        <div class="thumb">
                            <img src="img/about-img2.jpg" alt="">
                        </div>
                        <a href="#">
                            <h4>Gourmet Burgers</h4>
                        </a>
                        <!--<p>
									inappropriate behavior is often laughed off as “boys will be boys,” women face higher conduct women face higher conduct.
								</p>!-->
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="single-service">
                        <div class="thumb">
                            <img src="img/about-img3.jpg" alt="">
                        </div>
                        <a href="#">
                            <h4>Fresh & Healthy Salads</h4>
                        </a>
                        <!--<p>
									inappropriate behavior is often laughed off as “boys will be boys,” women face higher conduct women face higher conduct.
								</p>!-->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End services Area -->


    <!-- Start people-behind Area -->
    <section class="home-about-area section-gap">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 home-about-left">
                    <h1>The people behind Chickycharchar</h1>
                    <p>
                        ChickyCharChar is the dream realisation of owner operators, John and Vivian. With a long history in the hospitality industry, ChickyCharChar has been the result of years of planning, experience and the passion to deliver the best customer experience and modern healthy food without compromise. ChickyCharChar as it stands today is a team effort between the operators John, Vivian and some of their closest family and friends – that’s why when you walk into ChickyCharChar you will feel warmth and a welcome like that of family. The owners all agree on one thing: providing a terrific environment filled with energy and great food is just the beginning of an experience you will never forget and will keep you coming back.
                    </p>

                </div>
                <div class="col-lg-6 home-about-right">
                    <img class="img-fluid" src="img/about-behind.jpg" alt="">
                </div>
            </div>
        </div>
    </section>
    <!-- End people-behind Area -->

   <footer class="footer-area">
        <?php include_once'footer.php';?>
    </footer>
</body>

</html>
