<?php 
error_reporting(0);
include "../admin_includes/config.php";
include "../admin_includes/common_functions.php";
//Set Array for list
$lists = array();
$response = array();
if($_SERVER['REQUEST_METHOD']=='POST'){
	$result = getAllDataWithStatus('banners','0');
	if ($result->num_rows > 0) {
			$response["lists"] = array();
			while($row = $result->fetch_assoc()) {
				//Chedck the condioton for emptty or not		
				$lists = array();
		    	$lists["id"] = $row["id"];
		    	$lists["title"] = $row["title"];
                $lists["category_id"] = $row["service_category_id"];		    	
		    	$lists["image"] = $base_url."uploads/banner_images/".$row["banner"];
			array_push($response["lists"], $lists);		 
			}
			$response["success"] = RESCODE_SUCCESS;
			$response["message"] = MSG_SUCCESS;			
	} else {
	    $response["success"] = RESCODE_ERROR;
		$response["message"] = MSG_INVALID_ERROR;	   
	}
} else {
	$response["success"] = RESCODE_INVALID;
	$response["message"] = MSG_INVALID_REQUEST;
}
echo json_encode($response);

?>