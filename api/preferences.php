<?php 
error_reporting(0);
include "../admin_includes/config.php";
include "../admin_includes/common_functions.php";
//Set Array for list
$lists = array();
$response = array();
if($_SERVER['REQUEST_METHOD']=='POST'){
	$result = getAllDataWithStatus('preferences','0');
	if ($result->num_rows > 0) {
			$response["lists"] = array();
			while($row = $result->fetch_assoc()) {
				//Chedck the condioton for emptty or not		
				$lists = array();
		    	$lists["id"] = $row["id"];
		    	$lists["title"] = $row["name"];
                $lists["detail"] = $row["detail"];		    	
		    	$lists["image"] = $base_url."uploads/preference_images/".$row["image"];
			    array_push($response["lists"], $lists);		 
			}
			$response["success"] = RESCODE_SUCCESS;
			$response["message"] = MSG_SUCCESS;			
	} else {
	    $response["success"] = RESCODE_ERROR;
		$response["message"] = MSG_INVALID_ERROR;	   
	}
} else {
	$response["success"] = RESCODE_INVALID;
	$response["message"] = MSG_INVALID_REQUEST;
}
echo json_encode($response);

?>