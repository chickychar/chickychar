<?php 
error_reporting(1);
include "../admin_includes/config.php";
include "../admin_includes/common_functions.php";
//Set Array for list
$lists = array();
$response = array();
if($_SERVER['REQUEST_METHOD']=='POST'){
	$data = json_decode(file_get_contents('php://input'), true);
	$mobile       = $data['mobile'];
	if (isset($mobile) && is_numeric($mobile)) {
		    $checkMobOrEmail = "SELECT * FROM users WHERE user_mobile = '$mobile' AND status=0";
			$checkUseCount = $conn->query($checkMobOrEmail);
			$getCnt = $checkUseCount->num_rows;
		    //Set variable for session
		    if($getCnt > 0) {
				$date =date("Y-m-d H:i:s");
				$form_data = array(
					'last_login' =>$date,
				);
				$sql=dbRowUpdate('users',$form_data," WHERE user_mobile='$mobile'");
				$row = getSingleRecord("users",'user_mobile',$mobile);
				$response["user_id"] = $row['id'];
				$response["user_mobile"] = $row['user_mobile'];
		    	$response["success"] = RESCODE_SUCCESS;
				$response["message"] = MSG_SUCCESS;
			} else {
				$response["success"] = RESCODE_NORECORDS;
				$response["message"] = MSG_RECORDEXISTUSER;
			}	
	} else {
		$response["success"] = RESCODE_PARAMETERMISSING;
		$response["message"] = MSG_FIELD_MISSING;
	}
} else {
	$response["success"] = RESCODE_INVALID;
	$response["message"] = MSG_INVALID_REQUEST;
}
echo json_encode($response);

?>