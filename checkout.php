<!DOCTYPE html>
<html lang="zxx" class="no-js">

<head>
    <?php include_once'metahead.php';?>
</head>

<body>
    <header id="header">
      <?php include_once'header.php';?>
    </header><!-- #header -->

    <!-- start banner Area -->
    <section class="about-banner relative">
        <div class="overlay overlay-bg"></div>
        <div class="container">
            <div class="row d-flex align-items-center justify-content-center">
                <div class="about-content col-lg-12">
                    <h1 class="text-white">
                        Menus
                    </h1>
                    <p class="text-white link-nav"><a href="index.html">Home </a> <span class="lnr lnr-arrow-right"></span> <a href="menu.html"> Menus</a></p>
                </div>
            </div>
        </div>
    </section>
    <!-- End banner Area -->

    <section class="section section-sm section-first bg-default text-md-left">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 mb-sm-35 mb-xs-35">

                    <!--=======  Grid list view  =======-->
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-7 col-md-7 col-sm-12 col-12 pt-4">
                                <div class="row b-we p-3 rounded">
                                    <div class="col-lg-7 col-md-7 col-sm-12 col-12 checkout-asap">
                                        <h2>Logged In</h2>
                                        <p>Lancius | 8989898989</p>


                                    </div>

                                </div>
                                <div class="row mt-30 shadow-sm p-3 b-we rounded">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-12 ">

                                        <h2>Select Delivery Address</h2>
                                    </div>
                                    <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12 p-3 ">
                                        <address class="address border p-3 rounded small">
                                            <h4><span class="icon_pin_alt"></span>Add New Address</h4>
                                            <p>Shop 1, 145 McEvoy Street Alexandria NSW 2015

                                                02 9318 2000</p>
                                        </address>

                                    </div>
                                    <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12 p-3 ">
                                        <address class="border p-3 rounded small">
                                            <h4><span class="icon_pin_alt"></span>Add New Address</h4>
                                            <p class="text-center" style="font-size:30px;">+</p>
                                        </address>

                                    </div>
                                </div>
                                <div class="row mt-30 mb-3">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-12 shadow-sm p-3 b-we rounded">

                                        <div class="my-account-section section position-relative mb-50 fix">

                                            <div class="container">
                                                <div class="row">
                                                    <div class="col-12">

                                                        <div class="row">

                                                            <!-- My Account Tab Menu Start -->
                                                            <div class="col-lg-4 col-12">
                                                                <h2 class="pb-4">Payment</h2>
                                                                <div class="myaccount-tab-menu nav" role="tablist">
                                                                    <a href="#dashboad" class="" data-toggle="tab"><i class="fa fa-dashboard"></i>
                                                                        Wallet</a>


                                                                    <a href="#account-info" data-toggle="tab" class="active show"><i class="fa fa-user"></i>Pay On Delivery</a>



                                                                </div>
                                                            </div>
                                                            <!-- My Account Tab Menu End -->

                                                            <!-- My Account Tab Content Start -->
                                                            <div class="col-lg-8 col-12">
                                                                <div class="tab-content" id="myaccountContent">
                                                                    <!-- Single Tab Content Start -->
                                                                    <div class="tab-pane fade" id="dashboad" role="tabpanel">


                                                                        <div class="myaccount-content mt-60">
                                                                            <div class="row">
                                                                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-3 col-3">
                                                                                    <p>We Accept</p>
                                                                                </div>
                                                                                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2">
                                                                                    <img src="img/visa.png">
                                                                                </div>
                                                                                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2">
                                                                                    <img src="img/m.png">
                                                                                </div>
                                                                                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2">
                                                                                    <img src="img/m2.png">
                                                                                </div>
                                                                                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2">
                                                                                    <img src="img//m3.png">
                                                                                </div>
                                                                            </div>
                                                                            <a href="orderconfirm.php"><button class="cart-btn primary mt-2">Pay</button>
                                                                            </a>


                                                                        </div>

                                                                    </div>
                                                                    <!-- Single Tab Content End -->


                                                                    <!-- Single Tab Content Start -->
                                                                    <div class="tab-pane fade mt-45 active show" id="account-info" role="tabpanel">



                                                                        <div class="myaccount-content mt-60">
                                                                            <img src="img/cash-on.png">
                                                                        </div>
                                                                        <div class="myaccount-content mt-60">
                                                                            <h5>Cash</h5>
                                                                            <p class="small">Please keep exact change handy to help us serve you better</p>

                                                                            <a href="orderconfirm.php"><button class="cart-btn primary mt-2">Pay</button></a>
                                                                        </div>

                                                                    </div>
                                                                    <!-- Single Tab Content End -->
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-lg-5 col-md-5 col-sm-12 col-12 pt-4">
                                <div class=" checkout-cart-total cart-w rounded">

                                    <h2 class=" ">Cart</h2>
                                    <div class="row mb-20">
                                        <div class="col-xl-4 col-lg-6 col-md-8 col-sm-6 col-6">

                                            <img class="img-fluid rounded" src="img/menu/chik.png">
                                        </div>
                                        <div class="col-xl-8 col-lg-6 col-md-8 col-sm-6 col-6">
                                            <p>Burger King</p><small>Madhapur,Hyderabad</small>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6 col-sm-4 col-4">Palak Paneer</div>
                                        <div class="col-lg-3 col-sm-4 col-4"><button type="button" class="btn btn-light add-btn">- 1 +</button></div>
                                        <div class="col-lg-3 col-sm-4 col-4"><span>$25.00</span></div>
                                    </div>
                                    <div class="container mt-20 mb-20">
                                        <input class="form-control form-c" placeholder="Any suggestions? we will pass it on">
                                    </div>
                                    <ul>

                                        <li>Item total<span>$50.00</span></li>
                                        <li>Restaurant Charges<span>$4.00</span></li>

                                        <li class="border-bottom"></li>
                                        <li>Delivery Fee<span>$50.00</span></li>
                                        <li class="border-bottom"></li>
                                        <li>TO PAY<span>$50.00</span></li>



                                    </ul>


                                </div>
                            </div>
                            <!--=======  End of Grid list view  =======-->



                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
   <footer class="footer-area">
        <?php include_once'footer.php';?>
    </footer>


    <script>
        $(document).ready(function() {
            // Add scrollspy to <body>
            $('.category-menu').scrollspy({
                target: ".category-menu",
                offset: 50
            });

            // Add smooth scrolling on all links inside the navbar
            $("#myScrollspy a").on('click', function(event) {
                // Make sure this.hash has a value before overriding default behavior
                if (this.hash !== "") {
                    // Prevent default anchor click behavior
                    event.preventDefault();

                    // Store hash
                    var hash = this.hash;

                    // Using jQuery's animate() method to add smooth page scroll
                    // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
                    $('html, body').animate({
                        scrollTop: $(hash).offset().top
                    }, 900, function() {

                        // Add hash (#) to URL when done scrolling (default click behavior)
                        window.location.hash = hash;
                    });
                } // End if
            });
        });

    </script>


</body>

</html>
