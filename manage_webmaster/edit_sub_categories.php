<?php include_once 'admin_includes/main_header.php'; ?>
<?php
$id = $_GET['bid'];
if (!isset($_POST['submit']))  {
  echo "fail";
} else  {
  $cat_id = $_POST['cat_id'];
  $sub_category_name = $_POST['sub_category_name'];
  $sub_cat_image = $_FILES["fileToUpload"]["name"];
  if($_FILES["fileToUpload"]["name"]!='') {
    $target_dir = "../uploads/sub_category_images/";
    $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
      $sql = "UPDATE sub_categories SET cat_id = '$cat_id',sub_category_name = '$sub_category_name', sub_cat_image='$sub_cat_image' WHERE id = '$id' ";
    } else {
      echo "Sorry, there was an error uploading your file.";
    }
  } else {
    $sql = "UPDATE sub_categories SET cat_id='$cat_id',sub_category_name = '$sub_category_name' WHERE id = '$id' ";
  } 
  if($conn->query($sql) === TRUE){
    echo "<script type='text/javascript'>window.location='sub_categories.php?msg=success'</script>";
  } else {
      echo "<script type='text/javascript'>window.location='sub_categories.php?msg=fail'</script>";
  }
}
?>
<?php $getSubCategoriesData = getDataFromTables('sub_categories',$status=NULL,'id',$id,$activeStatus=NULL,$activeTop=NULL);
$getSubCategories = $getSubCategoriesData->fetch_assoc();
 ?>
<div class="site-content">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="m-y-0">Sub Categories</h3>
          </div>
          <div class="panel-body">            
            <div class="row">
              <div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                <form data-toggle="validator" method="post" enctype="multipart/form-data">
                <?php $getCategories = getAllDataWithStatus('categories','0');?>
                  <div class="form-group">
                    <label for="form-control-3" class="control-label">Choose Category</label>
                    <select id="form-control-3" name="cat_id" class="custom-select" data-error="This field is required." required>
                      <option value="">Select Category</option>
                      <?php while($row = $getCategories->fetch_assoc()) {  ?>
                          <option <?php if($row['id'] == $getSubCategories['cat_id']) { echo "Selected"; } ?> value="<?php echo $row['id']; ?>"><?php echo $row['category_name']; ?></option>
                      <?php } ?>
                   </select>
                    <div class="help-block with-errors"></div>
                  </div>               
                  <div class="form-group">
                    <label for="form-control-2" class="control-label">Sub Category Name</label>
                    <input type="text" class="form-control" id="form-control-2" name="sub_category_name" required value="<?php echo $getSubCategories['sub_category_name'];?>">
                    <div class="help-block with-errors"></div>
                  </div>
                  <div class="form-group">
                    <label for="form-control-4" class="control-label">Sub Category Image</label>
                    <img src="<?php echo $base_url . 'uploads/sub_category_images/'.$getSubCategories['sub_cat_image'] ?>"  id="output" height="100" width="100"/>
                    <label class="btn btn-default file-upload-btn">
                        Choose file...
                        <input id="form-control-22" class="file-upload-input" type="file" accept="image/*" name="fileToUpload" id="fileToUpload"  onchange="loadFile(event)"  multiple="multiple" >
                      </label>
                  </div>
                  <button type="submit" name="submit" value="Submit"  class="btn btn-primary btn-block">Submit</button>
                </form>
              </div>
            </div>
            <hr>
          </div>
        </div>
      </div>
      <?php include_once 'admin_includes/footer.php'; ?>
   <script src="js/tables-datatables.min.js"></script>

