<?php include_once 'admin_includes/main_header.php'; ?>
<?php
$id = $_GET['bid'];
if (!isset($_POST['submit']))  {
  echo "fail";
} else  {
    $cat_id = $_POST['cat_id'];
    $sub_cat_id = $_POST['sub_cat_id'];
    $sub_sub_cat_name = $_POST['sub_sub_cat_name'];
    $sub_sub_cat_image = $_FILES["fileToUpload"]["name"];
    
    if($_FILES["fileToUpload"]["name"]!='') {
        $target_dir = "../uploads/sub_sub_category_images/";
        $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file);
        $sql = "UPDATE sub_sub_categories SET cat_id = '$cat_id',sub_cat_id = '$sub_cat_id',sub_sub_cat_name = '$sub_sub_cat_name', sub_sub_cat_image='$sub_sub_cat_image' WHERE id = '$id' ";
    } else {
        $sql = "UPDATE sub_sub_categories SET cat_id='$cat_id',sub_cat_id = '$sub_cat_id',sub_sub_cat_name = '$sub_sub_cat_name' WHERE id = '$id' ";
    } 
    if($conn->query($sql) === TRUE){
        echo "<script type='text/javascript'>window.location='sub_sub_categories.php?msg=success'</script>";
    } else {
        echo "<script type='text/javascript'>window.location='sub_sub_categories.php?msg=fail'</script>";
    }
}
?>
<?php $getSubSubCategories = getDataFromTables('sub_sub_categories',$status=NULL,'id',$id,$activeStatus=NULL,$activeTop=NULL);
$getSubSubCategoriesData = $getSubSubCategories->fetch_assoc();
 ?>
<div class="site-content">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="m-y-0">Sub SUb Categories</h3>
          </div>
          <div class="panel-body">            
            <div class="row">
              <div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                <form data-toggle="validator" method="post" enctype="multipart/form-data">
                <?php $getCategories = getAllDataWithStatus('categories','0');?>
                  <div class="form-group">
                    <label for="form-control-3" class="control-label">Choose Category</label>
                    <select id="form-control-3" name="cat_id" class="custom-select cat_id" data-error="This field is required." required>
                      <option value="">Select Category</option>
                      <?php while($row = $getCategories->fetch_assoc()) {  ?>
                          <option <?php if($row['id'] == $getSubSubCategoriesData['cat_id']) { echo "Selected"; } ?> value="<?php echo $row['id']; ?>"><?php echo $row['category_name']; ?></option>
                      <?php } ?>
                   </select>
                    <div class="help-block with-errors"></div>
                  </div> 
                  <?php $getSubSubCategoris = getAllDataWithStatus('sub_categories','0');?>  
                  <div class="form-group">
                    <label for="form-control-3" class="control-label">Choose your Sub Category</label>
                    <select id="form-control-3" name="sub_cat_id" class="custom-select sub_cat_id" data-error="This field is required." required >
                      <option value="">Select Sub Category</option>
                      <?php while($row = $getSubSubCategoris->fetch_assoc()) {  ?>
                          <option <?php if($row['id'] == $getSubSubCategoriesData['sub_cat_id']) { echo "Selected"; } ?> value="<?php echo $row['id']; ?>"><?php echo $row['sub_category_name']; ?></option>
                      <?php } ?>
                    </select>
                      <div class="help-block with-errors"></div>
                  </div>            
                  <div class="form-group">
                    <label for="form-control-2" class="control-label">Sub Sub Category Name</label>
                    <input type="text" class="form-control" id="form-control-2" name="sub_sub_cat_name" required value="<?php echo $getSubSubCategoriesData['sub_sub_cat_name'];?>">
                    <div class="help-block with-errors"></div>
                  </div>
                  <div class="form-group">
                    <label for="form-control-4" class="control-label">Sub Sub Category Image</label>
                    <img src="<?php echo $base_url . 'uploads/sub_sub_category_images/'.$getSubSubCategoriesData['sub_sub_cat_image'] ?>"  id="output" height="100" width="100"/>
                    <label class="btn btn-default file-upload-btn">
                        Choose file...
                        <input id="form-control-22" class="file-upload-input" type="file" accept="image/*" name="fileToUpload" id="fileToUpload"  onchange="loadFile(event)"  multiple="multiple" >
                      </label>
                  </div>
                  <button type="submit" name="submit" value="Submit"  class="btn btn-primary btn-block">Submit</button>
                </form>
              </div>
            </div>
            <hr>
          </div>
        </div>
      </div>
      <?php include_once 'admin_includes/footer.php'; ?>
   <script src="js/tables-datatables.min.js"></script>
   <script>
    $("body").on("change",".cat_id", function (e) {
        var id =$(this).val();
        $.ajax({
        type: "POST",
        url: "get_sub_categories.php",
        data:'category_id='+id,
        success: function(data){
            $(".sub_cat_id").html(data);
        }
        });
    });
    </script>
