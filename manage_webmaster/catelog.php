<?php include_once 'admin_includes/main_header.php'; ?>
<?php $getcateringData = getAllDataWithActiveRecent('catering'); $i=1; ?>
     <div class="site-content">
        <div class="panel panel-default panel-table">
          <div class="panel-heading">
            <a href="add_catelog.php" style="float:right">Add Catelog</a>
            <h3 class="m-t-0 m-b-5">Catelog</h3>            
          </div>
          <div class="panel-body">
            <div class="table-responsive">
              <table class="table table-striped table-bordered dataTable" id="table-1">
                <thead>
                  <tr>
                    <th>S.No</th>
                    <th>Category Name</th>
                    <th>Sub Category Name</th>
                    <th>Product Name</th>
                    <th>Banner</th>
                    <th>Status</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  <?php while ($row = $getcateringData->fetch_assoc()) { ?>
                  <tr>
                     <td><?php echo $i;?></td>
                    <td><?php $getCategoriesData = getAllData('categories'); while($getCategories = $getCategoriesData->fetch_assoc()) { if($row['cat_id'] == $getCategories['id']) { echo $getCategories['category_name']; } } ?></td>

                    <td><?php $getSubCategoriesData = getAllData('sub_categories'); 
                    while($getSubCategories = $getSubCategoriesData->fetch_assoc()) { if($row['subcat_id'] == $getSubCategories['id']) { echo $getSubCategories['sub_category_name']; } } ?></td>
                    <td><?php echo $row['product_name'];?></td>
                    <td><img src="<?php echo $base_url . 'uploads/catelog_images/'.$row['image'] ?>" height="100" width="100"/></td>
                    <td><?php if ($row['status']==0) { echo "<span class='label label-outline-success check_active open_cursor' data-incId=".$row['id']." data-status=".$row['status']." data-tbname='catering'>Active</span>" ;} else { echo "<span class='label label-outline-info check_active open_cursor' data-status=".$row['status']." data-incId=".$row['id']." data-tbname='catering'>In Active</span>" ;} ?></td>
                    <td> <a href="edit_catelog.php?bid=<?php echo $row['id']; ?>"> <i class="zmdi zmdi-edit"></i> &nbsp; </a> <!-- <a href="delete_banners.php?bid=<?php echo $row['id']; ?>"><i class="zmdi zmdi-delete zmdi-hc-fw" onclick="return confirm('Are you sure you want to delete?')"></i></a> --></td>
                  </tr>
                  <?php  $i++; } ?>                  
                </tbody>               
              </table>
            </div>
          </div>
        </div>   
       </div>
   <?php include_once 'admin_includes/footer.php'; ?>
   <script src="js/tables-datatables.min.js"></script>