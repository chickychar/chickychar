<?php include_once 'admin_includes/main_header.php'; ?>
      <div class="site-content">
        <div class="row">
          <!-- <a href="admin_users.php" style="color: white;text-decoration: none !important;">
          <div class="col-md-4 col-sm-5">
            <div class="widget widget-tile-2 bg-warning m-b-30">
              <div class="wt-content p-a-20 p-b-50">
                <div class="wt-title">Admin Users</div>
                <div class="wt-number"><?php echo getRowsCount('admin_users')?></div>
              </div>
              <div class="wt-icon">
                <i class="zmdi zmdi-accounts"></i>
              </div>
            </div>
          </div>
          </a> -->
          <?php 

        $getNewOrders = "SELECT * FROM orders WHERE order_status=8 AND payment_status!=3 AND DATE(`order_date`) = CURDATE() GROUP BY order_id ORDER BY id DESC";
        $getNewOrders1 = $conn->query($getNewOrders);
        $getNewOrders2 = $getNewOrders1->num_rows;
        
      ?>
      <?php 

        $getCompletedOrders = "SELECT * FROM orders WHERE order_status=3 AND payment_status!=3 AND DATE(`order_date`) = CURDATE() GROUP BY order_id ORDER BY id DESC";
        $getCompletedOrders1 = $conn->query($getCompletedOrders);
        $getCompletedOrders2 = $getCompletedOrders1->num_rows;
        
        
      ?>
      <?php 

        $getProcessingOrders = "SELECT * FROM orders WHERE order_status=1 AND payment_status!=3 AND DATE(`order_date`) = CURDATE() GROUP BY order_id ORDER BY id DESC";
        $getProcessingOrders1 = $conn->query($getProcessingOrders);
        $getProcessingOrders2 = $getProcessingOrders1->num_rows;
        
      ?>
      <?php 

        $getTakeAwayOrders = "SELECT * FROM orders WHERE order_status=7 AND DATE(`order_date`) = CURDATE() GROUP BY order_id DESC";
        $getTakeAwayOrders1 = $conn->query($getTakeAwayOrders);
        $getTakeAwayOrders2 = $getTakeAwayOrders1->num_rows;
        
      ?>
       <?php 
              /*$ord_status = $getNewProducts2['order_status'];
              $sql = "SELECT * FROM order_status WHERE id = '$ord_status'";
              $result = $conn->query($sql);
              $noRows = $result->num_rows;*/
       ?>
       
          <a href="orders.php">
          <div class="col-md-4 col-sm-4">
            <div class="widget widget-tile-2 bg-danger m-b-30">
              <div class="wt-content p-a-20 p-b-50">
                <div class="wt-title">New Orders</div>
                <div class="wt-number"><?php echo $getNewOrders2;?></div>
              </div>
              <div class="wt-icon">
                <i class="zmdi zmdi-shopping-cart-plus zmdi-hc-fw"></i>
              </div>
            </div>
          </div>
          </a>
          <a href="today_orders.php">
          <div class="col-md-4 col-sm-4">
            <div class="widget widget-tile-2 bg-warning m-b-30">
              <div class="wt-content p-a-20 p-b-50">
                <div class="wt-title">Orders In Process</div>
                <div class="wt-number"><?php echo $getProcessingOrders2; ?></div>
              </div>
              <div class="wt-icon">
                <i class="zmdi zmdi-shopping-cart-plus zmdi-hc-fw"></i>
              </div>
            </div>
          </div>
          </a>
          <a href="completed_orders.php">
          <div class="col-md-4 col-sm-4">
            <div class="widget widget-tile-2 bg-primary m-b-30">
              <div class="wt-content p-a-20 p-b-50">
                <div class="wt-title">Completed Orders</div>
                <div class="wt-number"><?php echo $getCompletedOrders2; ?></div>
              </div>
              <div class="wt-icon">
                <i class="zmdi zmdi-shopping-cart-plus zmdi-hc-fw"></i>
              </div>
            </div>
          </div>
          </a>
          

        </div>
        <h3 style="text-align:center;color:green;">Take Away Orders</h3>
        <div class="row">
          <?php 

        $getTakeAwayOrders = "SELECT * FROM orders WHERE order_status=7 AND DATE(`order_date`) = CURDATE() GROUP BY order_id DESC";
        $getTakeAwayOrders1 = $conn->query($getTakeAwayOrders);
        $getTakeAwayOrders2 = $getTakeAwayOrders1->num_rows;
        
      ?>
          <a href="take_away_orders.php">
          <div class="col-md-4 col-sm-4">
            <div class="widget widget-tile-2 bg-primary m-b-30">
              <div class="wt-content p-a-20 p-b-50">
                <div class="wt-title">Take A Way Orders</div>
                <div class="wt-number"><?php echo $getTakeAwayOrders2; ?></div>
              </div>
              <div class="wt-icon">
                <i class="zmdi zmdi-shopping-cart-plus zmdi-hc-fw"></i>
              </div>
            </div>
          </div>
          </a>
        </div>
        <?php 

        $getOrdersPlaced = "SELECT * FROM orders WHERE order_status=1  ORDER BY id DESC";
        $getOrdersPlaced1 = $conn->query($getOrdersPlaced);
        $getOrdersPlaced2 = $getOrdersPlaced1->num_rows;
        
      ?>
      <?php 

        $getOrdersDelivered = "SELECT * FROM orders WHERE order_status=2  ORDER BY id DESC";
        $getOrdersDelivered1 = $conn->query($getOrdersDelivered);
        $getOrdersDelivered2 = $getOrdersDelivered1->num_rows;
        
      ?>
      <?php 

        $getOrdersCancelled = "SELECT * FROM orders WHERE order_status=3 ORDER BY id DESC";
        $getOrdersCancelled1 = $conn->query($getOrdersCancelled);
        $getOrdersCancelled2 = $getOrdersCancelled1->num_rows;
        
      ?>
        <h3 style="text-align:center;color:green;">Total Number Of Orders</h3>
        <div class="row">
          
          <a href="orders.php">
          <div class="col-md-4 col-sm-4">
            <div class="widget widget-tile-2 bg-warning m-b-30">
              <div class="wt-content p-a-20 p-b-50">
                <div class="wt-title">Orders Placed</div>
                <div class="wt-number"><?php echo $getOrdersPlaced2; ?></div>
              </div>
              <div class="wt-icon">
                <i class="zmdi zmdi-shopping-cart-plus zmdi-hc-fw"></i>
              </div>
            </div>
          </div>
          </a>
          <a href="delivered_orders.php">
          <div class="col-md-4 col-sm-4">
            <div class="widget widget-tile-2 bg-primary m-b-30">
              <div class="wt-content p-a-20 p-b-50">
                <div class="wt-title">Orders Delivered</div>
                <div class="wt-number"><?php echo $getOrdersDelivered2; ?></div>
              </div>
              <div class="wt-icon">
                <i class="zmdi zmdi-shopping-cart-plus zmdi-hc-fw"></i>
              </div>
            </div>
          </div>
          </a>
          <a href="cancelled_orders.php">
          <div class="col-md-4 col-sm-4">
            <div class="widget widget-tile-2 bg-danger m-b-30">
              <div class="wt-content p-a-20 p-b-50">
                <div class="wt-title">Orders Cancelled</div>
                <div class="wt-number"><?php echo $getOrdersCancelled2; ?></div>
              </div>
              <div class="wt-icon">
                <i class="zmdi zmdi-shopping-cart-plus zmdi-hc-fw"></i>
              </div>
            </div>
          </div>
          </a>

        </div>
        
         <?php /*if($noRows!=0) {*/ ?>
        <!-- <div class="col-md-6 m-b-30">
          <h4 class="m-t-0 m-b-30">Pie chart</h4>
          <div id="pie" style="height: 300px"></div>
        </div> -->
        <?php /*}*/ ?>

      </div>
      
     <?php include_once 'admin_includes/footer.php'; ?>
      <script type="text/javascript">
    "use strict";!function(a){new Chart(a("#infoblock-chart-1"),{type:"line",data:{labels:["January","February","March","April","May","June","July"],datasets:[{label:"Dataset",data:[45,40,30,20,25,35,50],fill:!0,backgroundColor:"#e53935",borderColor:"#e53935",borderWidth:2,borderCapStyle:"butt",borderDash:[],borderDashOffset:0,borderJoinStyle:"miter",pointBorderColor:"#e53935",pointBackgroundColor:"#fff",pointBorderWidth:2,pointHoverRadius:4,pointHoverBackgroundColor:"#e53935",pointHoverBorderColor:"#fff",pointHoverBorderWidth:2,pointRadius:[0,4,4,4,4,4,0],pointHitRadius:10,spanGaps:!1}]},options:{scales:{xAxes:[{display:!1}],yAxes:[{display:!1,ticks:{min:0,max:60}}]},legend:{display:!1}}}),new Chart(a("#infoblock-chart-2"),{type:"line",data:{labels:["January","February","March","April","May","June","July"],datasets:[{label:"Dataset",data:[30,22,18,25,40,55,60],fill:!0,backgroundColor:"#7d57c1",borderColor:"#7d57c1",borderWidth:2,borderCapStyle:"butt",borderDash:[],borderDashOffset:0,borderJoinStyle:"miter",pointBorderColor:"#7d57c1",pointBackgroundColor:"#fff",pointBorderWidth:2,pointHoverRadius:4,pointHoverBackgroundColor:"#7d57c1",pointHoverBorderColor:"#fff",pointHoverBorderWidth:2,pointRadius:[0,4,4,4,4,4,0],pointHitRadius:10,spanGaps:!1}]},options:{scales:{xAxes:[{display:!1}],yAxes:[{display:!1,ticks:{min:0,max:60}}]},legend:{display:!1}}}),a('[data-chart="peity"]').each(function(){var b=a(this).attr("data-type");a(this).peity(b)}),Morris.Donut({element:"donut1",data:[{label:"Android",value:<?php echo $getAndroidUsersCount; ?>},{label:"iOS",value:<?php echo $getIosUsersCount; ?>},{label:"WebUsers",value:<?php echo $getWindowsUsersCount; ?>}],resize:!0,colors:["#1d87e4","#faa800","#e53935"]}),a("#vector-map").vectorMap({map:"world_en",backgroundColor:null,borderColor:null,borderOpacity:.5,borderWidth:1,color:"#1d87e4",enableZoom:!0,hoverColor:"#1d87e4",hoverOpacity:.7,normalizeFunction:"linear",selectedColor:"#faa800",selectedRegions:["au","ca","de","br","in"],showTooltip:!0});for(var b=[],c=0;c<=6;c+=1)b.push([c,parseInt(20*Math.random())]);for(var d=[],e=0;e<=6;e+=1)d.push([e,parseInt(20*Math.random())]);var f=[{label:"Data One",data:b,bars:{order:1}},{label:"Data Two",data:d,bars:{order:2}}];a.plot(a("#chart-bar"),f,{bars:{show:!0,barWidth:.2,fill:1},series:{stack:0},grid:{color:"#aaa",hoverable:!0,borderWidth:0,labelMargin:5,backgroundColor:"#fff"},legend:{show:!1},colors:["#faa800","#34a853"],tooltip:!0,tooltipOpts:{content:"%s : %y.0",shifts:{x:-30,y:-50}}}),a(function(){function b(){for(d.length>0&&(d=d.slice(1));d.length<e;){var a=d.length>0?d[d.length-1]:50,b=a+10*Math.random()-5;b<5?b=5:b>95&&(b=95),d.push(b)}for(var c=[],f=0;f<d.length;++f)c.push([f,d[f]]);return c}function c(){g.setData([b()]),g.draw(),setTimeout(c,f)}var d=[],e=300,f=60,g=a.plot("#realtime",[b()],{series:{shadowSize:0},yaxis:{min:0,max:100},xaxis:{min:0,max:300},colors:["#7d57c1"],grid:{color:"#aaa",hoverable:!0,borderWidth:0,backgroundColor:"#fff"},tooltip:!0,tooltipOpts:{content:"Y: %y",defaultTheme:!1}});c()})}(jQuery);

    </script>
     <script src="js/charts-flot.min.js"></script>
     <script type="text/javascript">
            /*$(document).ready(function() {
                
                var pie = function () {
                    var data = [
                      {
                        label: "Orders",
                        data: <?php echo getRowsCount('orders')?>,
                        color: "#34a853",
                      }, 
                      {
                        label: "Number Of Deliveries",
                        data: <?php echo $noRows;?>,
                        color: "#7d57c1",
                    }];
                    var options = {
                        series: {
                            pie: {
                                show: true
                            }
                        },
                        legend: {
                            labelFormatter: function(label, series){
                                return '<span class="pie-chart-legend">'+label+'</span>';
                            }
                        },
                        grid: {
                            hoverable: true
                        },
                        tooltip: true,
                        tooltipOpts: {
                            content: "%p.0%, %s",
                            shifts: {
                                x: 20,
                                y: 0
                            },
                            defaultTheme: false
                        }
                    };
                    $.plot($("#pie"), data, options);
                };

                pie();
               
            });*/
        </script>