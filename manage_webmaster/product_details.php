<?php include_once 'admin_includes/main_header.php'; ?>
<?php
$product_code = $_GET['pcode'];
$query = getAllDataWhere('products','product_code',$product_code); 
$query1 = getAllDataWhere('product_details','product_code',$product_code);
$products = $query->fetch_assoc();
$category = getSingleColumnName($products['cat_id'],'id','category_name','categories');
$sub_category = getSingleColumnName($products['subcat_id'],'id','sub_category_name','sub_categories');
$i=1; 
$product_name = getSingleColumnName($product_code,'product_code','product_name','products');?>
<div class="site-content">
  <div class="panel panel-default panel-table">
    <div class="panel-heading">
      <h3 class="m-t-0 m-b-5"><?php echo $product_name; ?></h3>
    </div>
    <div class="panel-body">
    <div class="card" id="profile-main">
        <div class="pmb-block">
            <div class="pmbb-body p-l-30">
                <div class="pmbb-view">
                    <dl class="dl-horizontal">
                        <dt>Product Name</dt>
                        <dd><?php echo $products['product_name']; ?></dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt>Category Name</dt>
                        <dd><?php echo $category; ?></dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt>Sub Category name</dt>
                        <dd><?php echo $sub_category; ?></dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt>Price</dt>
                        <dd><?php echo $products['product_price']; ?></dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt>Product Information</dt>
                        <dd><?php echo $products['product_info']; ?></dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt>Product Description</dt>
                        <dd><?php echo $products['product_description']; ?></dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt>Product Image</dt>
                        <dd><?php echo $products['product_image']; ?></dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt>Avaliability</dt>
                        <dd><?php if ($products['availability_id']==0) { echo "Availiable" ; } else { echo "Not avaliable"; } ?></dd>
                    </dl>
                </div>
            </div>
            <h4><i class="zmdi zmdi-equalizer m-r-10"></i>Product Details</h4>
            <div class="pmbb-body p-l-30">
                <div class="pmbb-view">
                    <?php while ($productDetails = $query1->fetch_assoc()) { 
                        $product_type = getSingleColumnName($productDetails['ptoduct_type_id'],'product_type_code','product_type','product_types');
                        $sql2 = getAllDataWhere('product_name_details','product_type_code',$productDetails['product_type_code']);
                    ?>
                        <dl class="dl-horizontal">
                            <dt>Product Type</dt>
                            <dd><?php echo $product_type;?></dd>
                        </dl>
                        <dl class="dl-horizontal">
                            <dt>Avaliability</dt>
                            <dd><?php if ($productDetails['availability_id']==0) { echo "Availiable" ; } else { echo "Not avaliable"; } ?></dd>
                        </dl>
                        <dl class="dl-horizontal">
                            <dt>Required</dt>
                            <dd><?php if ($productDetails['required_id']==0) { echo "In Stock" ; } else { echo "Sold Out"; } ?></dd>
                        </dl>
                        <dl class="dl-horizontal">
                            <dt>Choose Note</dt>
                            <dd><?php echo $productDetails['choose_note'];?></dd>
                        </dl>
                        <h5></i>Product Type Details</h5>
                        <table  class="table table-striped table-bordered dataTable" id="table-1">
                            <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Type Name</th>
                                    <th>Type Price</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i=1; while ($typeNames = $sql2->fetch_assoc()) { 
                                    $type_name = getSingleColumnName($typeNames['type_name_id'],'id','type_name','product_type_details');
                                    $type_price = getSingleColumnName($typeNames['type_name_id'],'id','type_price','product_type_details');
                                ?>
                                <tr>
                                    <td><?php echo $i;?></td>
                                    <td><?php echo $type_name;?></td>
                                    <td><?php if($type_price == "0") { echo '--'; } else { echo $type_price; } ?></td>
                                </tr>
                                <?php $i++; } ?>
                            </tbody> 
                        </table>   
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>
<?php include_once 'admin_includes/footer.php'; ?>