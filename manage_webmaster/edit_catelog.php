<?php include_once 'admin_includes/main_header.php'; ?>
<?php  
$id = $_GET['bid'];
if (!isset($_POST['submit']))  {
            echo "";
} else  {
    $cat_id = $_POST['cat_id'];
    $subcat_id = $_POST['subcat_id'];
    $product_name = $_POST['product_name'];
    $price_type_id = $_POST['price_type_id'];
    $regular_price = $_POST['regular_price'];
    $large_price = $_POST['large_price'];
    $minutes = $_POST['minutes'];
    $status = $_POST['status'];
    $total_price = $_POST['total_price'];

    if($price_type_id == 1) {
        $regular_price1 = $regular_price;
        $large_price1 =0;
        $minutes1 =0;
        $total_price1 =0;
    }
    if($price_type_id == 2){
        $regular_price1 = $regular_price;
        $large_price1 = $large_price;
        $minutes1 =0;
        $total_price1 =0;
    }

    if($price_type_id == 3){
        $regular_price1 = $regular_price;
        $minutes1 =$minutes;
        $total_price1 =$total_price;
        $large_price1 =0;
    }

      if($_FILES["fileToUpload"]["name"]!='') {
              $fileToUpload = $_FILES["fileToUpload"]["name"];
              $target_dir = "../uploads/catelog_images/";
              $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
              $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
              $getImgUnlink = getImageUnlink('image','catering','id',$id,$target_dir);
                //Send parameters for img val,tablename,clause,id,imgpath for image ubnlink from folder
        if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
      
             $sql1 = "UPDATE catering SET cat_id = '$cat_id',subcat_id = '$subcat_id',product_name = '$product_name',price_type_id ='$price_type_id',regular_price='$regular_price1',large_price='$large_price1', minutes ='$minutes1',image = '$fileToUpload', status = '$status',total_price = '$total_price1' WHERE id = '$id'";
                  $result = $conn->query($sql1);
                   if($result==1){
                echo "<script type='text/javascript'>window.location='catelog.php?msg=success'</script>";
            } else {
                echo "<script type='text/javascript'>window.location='catelog.php?msg=fail'</script>";
            }
                } else {
                    echo "Sorry, there was an error uploading your file.";
                }
      }  else {
                $sql2 = "UPDATE catering SET cat_id = '$cat_id',subcat_id = '$subcat_id',product_name = '$product_name',price_type_id ='$price_type_id',regular_price='$regular_price1',large_price='$large_price1', minutes ='$minutes1', status = '$status',total_price = '$total_price1' WHERE id = '$id'";
                //echo $sql2; die;

              $result1 = $conn->query($sql2);
               if($result1==1){
                echo "<script type='text/javascript'>window.location='catelog.php?msg=success'</script>";
            } else {
                echo "<script type='text/javascript'>window.location='catelog.php?msg=fail'</script>";
            }
        }          
      
            
            
          }
?>

      <div class="site-content">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="m-y-0">Edit Catelog</h3>
          </div>
          <div class="panel-body">
            <div class="row">
              <?php $getCateloggData = getDataFromTables('catering','0','id',$id,$activeStatus=NULL,$activeTop=NULL);
                $getCatelog = $getCateloggData->fetch_assoc();
                $getCategories = getDataFromTables('categories','0',$clause=NULL,$id=NULL,$activeStatus=NULL,$activeTop=NULL);
                ?>
                
              <div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                <form data-toggle="validator" method="post" enctype="multipart/form-data">

                  <div class="form-group">
                    <label for="form-control-3" class="control-label">Choose your Category</label>
                    <select id="form-control-3" name="cat_id" class="custom-select" data-error="This field is required." required onChange="getSubCategories(this.value);">
                      <option value="">Select Category</option>
                      <?php while($row = $getCategories->fetch_assoc()) {  ?>
                        <option value="<?php echo $row['id']; ?>" <?php if($row['id'] == $getCatelog['cat_id']) { echo "selected=selected"; }?> ><?php echo $row['category_name']; ?></option>
                    <?php } ?>
                   </select>
                    <div class="help-block with-errors"></div>
                  </div>
                  <?php $getSubCategories =  getDataFromTables('sub_categories',$status=NULL,$clause=NULL,$id=NULL,$activeStatus=NULL,$activeTop=NULL); ?>
                  <div class="form-group">
                    <label for="form-control-3" class="control-label">Select Product Type</label>
                    <select id="sub_category_id" name="subcat_id" class="custom-select" data-error="This field is required." required >
                       <option value="">Select Product Type</option>
                      <?php while($row = $getSubCategories->fetch_assoc()) {  ?>
                      <option <?php if($row['id'] == $getCatelog['subcat_id']) { echo "Selected"; } ?> value="<?php echo $row['id']; ?>"><?php echo $row['sub_category_name']; ?></option>
                      <?php } ?>
                   </select>
                    <div class="help-block with-errors"></div>
                  </div>
                  
                  <div class="form-group">
                    <label for="form-control-2" class="control-label">Product Name</label>
                    <input type="text" class="form-control" id="form-control-2" name="product_name" data-error="Please enter product name." required value="<?php echo $getCatelog['product_name']; ?>">
                    <div class="help-block with-errors"></div>
                  </div>
          
                 <div class="form-group">
                    <label for="form-control-4" class="control-label">Image</label>
                    <img src="<?php echo $base_url . 'uploads/catelog_images/'.$getCatelog['image'] ?>"  id="output" height="100" width="100"/>
                    <label class="btn btn-default file-upload-btn">
                        Choose file...
                        <input id="form-control-22" class="file-upload-input" type="file" accept="image/*" name="fileToUpload" id="fileToUpload"  onchange="loadFile(event)"  multiple="multiple" >
                      </label>
                  </div>
                  <div class="form-group">
                        <label for="form-control-3" class="control-label">Product Price Type</label>
                        <select id="price_type_id" name="price_type_id" class="custom-select" data-error="This field is required." required>
                                <option >Product Price Type</option>
                                <option value="1" <?php if($getCatelog['price_type_id'] == 1) { echo "Selected=Selected"; }?>>Normal</option>
                                <option value="2" <?php if($getCatelog['price_type_id'] == 2) { echo "Selected=Selected"; }?>>REG / LEG</option>
                                <option value="3" <?php if($getCatelog['price_type_id'] == 3) { echo "Selected=Selected"; }?>>Min</option>
                        </select>
                        <div class="help-block with-errors"></div>
                    </div>
                  <div class="form-group" id="actual_price_div">
                        <label for="form-control-2" class="control-label">Regular Price</label>
                        <input type="text" class="form-control" id="regular_price" name="regular_price" placeholder="Regular Price" data-error="Please Enter Regular Price." value="<?php echo $getCatelog['regular_price'];?>">
                        <div class="help-block with-errors"></div>
                    </div>

                    <div id="info_div">
                    <div class="form-group">
                        <label for="form-control-2" class="control-label">Larage Price</label>
                        <input type="text" class="form-control" id="large_price" name="large_price" placeholder="Larage Price" data-error="Please Enter Larage Price." value="<?php echo $getCatelog['large_price'];?>">
                        <div class="help-block with-errors"></div>
                    </div>
                    </div>
                    <div id="min_div">
                    <div class="form-group">
                        <label for="form-control-2" class="control-label">Minutes</label>
                        <input type="text" class="form-control" id="min_id" name="minutes" placeholder="Minutes" data-error="Please Enter Minutes." value="<?php echo $getCatelog['minutes'];?>">
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label for="form-control-2" class="control-label">Total Price</label>
                        <input type="text" class="form-control" id="total_price" name="total_price" placeholder="Minutes" data-error="Please Enter Minutes." readonly="readonly" value="<?php echo $getCatelog['total_price'];?>">
                        <div class="help-block with-errors"></div>
                    </div>
                    </div>
                  <?php $getStatus = getDataFromTables('user_status',$status=NULL,$clause=NULL,$id=NULL,$activeStatus=NULL,$activeTop=NULL);?>
                  <div class="form-group">
                    <label for="form-control-3" class="control-label">Choose your status</label>
                    <select id="form-control-3" name="status" class="custom-select" data-error="This field is required." required>
                      <option value="">Select Status</option>
                      <?php while($row = $getStatus->fetch_assoc()) {  ?>
                          <option <?php if($row['id'] == $getCatelog['status']) { echo "Selected"; } ?> value="<?php echo $row['id']; ?>"><?php echo $row['status']; ?></option>
                      <?php } ?>
                   </select>
                    <div class="help-block with-errors"></div>
                  </div>

                  <button type="submit" name="submit" value="Submit"  class="btn btn-primary btn-block">Submit</button>
                </form>
              </div>
            </div>
            <hr>
          </div>
        </div>
      </div>
      <?php include_once 'admin_includes/footer.php'; ?>
      <script src="//cdn.ckeditor.com/4.7.0/full/ckeditor.js"></script>
      <script src="js/multi_image_upload.js"></script>
      <link rel="stylesheet" type="text/css" href="css/multi_image_upload.css">
      <script>          
          /*CKEDITOR.replace( 'product_info' );
          CKEDITOR.replace( 'benefits' );
          CKEDITOR.replace( 'how_to_use' ); */          
      </script>
      <script type="text/javascript">
        function getSubCategories(val) { 
        $.ajax({
        type: "POST",
        url: "get_sub_categories.php",
        data:'category_id='+val,
        success: function(data){
            $("#sub_category_id").html(data);
        }
        });
    }
      </script>
<script type="text/javascript">
        $(document).ready(function () {
          $('#actual_price_div').hide();
          $('#info_div').hide();
          $('#min_div').hide();
          $('#price_type_id').change(function() {
             if($(this).val() == 1) {
                $('#actual_price_div').show();
                $('#info_div').hide();
                $('#min_div').hide();
                $("#regular_price").attr("required", "true");
             }else if($(this).val() == 2){
                $('#actual_price_div').show();
                $('#info_div').show();
                $('#min_div').hide();
                $("#regular_price,#large_price").removeAttr('required');
             } 
             else if($(this).val() == 3){
                $('#info_div').hide();
                $('#actual_price_div').show();
                $('#min_div').show();
                $("#regular_price,#min_id").removeAttr('required');
             }   
          });


          $('#min_id').change(function() {
             var minutes = $('#min_id').val();
             var regular_price = $('#regular_price').val();
             totalPrice = minutes * regular_price;
             $('#total_price').val(totalPrice);
          });
        });  
    </script>


<script src="//cdn.ckeditor.com/4.7.0/full/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'description' ); 
</script>
<style type="text/css">
    .cke_top, .cke_contents, .cke_bottom {
        border: 1px solid #333;
    }
</style>