<?php include_once 'admin_includes/main_header.php'; ?>	
<?php
if (!isset($_POST['submit']))  {
    echo "fail";
} else  { 
    $product_type = $_POST['product_type'];
    $type_name = $_REQUEST['type_name'];
    $product_type_code = rand(1234,9876);
    $sql = "INSERT INTO product_types (`product_type_code`,`product_type`) VALUES ('$product_type_code','$product_type')";
    $result = $conn->query($sql);
    foreach($type_name as $key=>$value){
        if(!empty($value)) {
            $type_name = $_REQUEST['type_name'][$key];    
            $type_price = $_REQUEST['type_price'][$key];
            $sql = "INSERT INTO product_type_details (`product_type_code`,`type_name`,`type_price`) VALUES ('$product_type_code','$type_name','$type_price')";
            $result = $conn->query($sql);
        }
    }
    if( $result == 1){
        echo "<script type='text/javascript'>window.location='add_product_type.php?msg=success'</script>";
    } else {
        echo "<script type='text/javascript'>window.location='add_product_type.php?msg=fail'</script>";
    }
}
?>
      <div class="site-content">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="m-y-0">Product Types</h3>
          </div>
          <div class="panel-body">
            <div class="row">
              <div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                <form data-toggle="validator" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="form-control-2" class="control-label">Product Type</label>
                        <input type="text" class="form-control" id="product_type" name="product_type" placeholder="Product Name" data-error="Please enter product type." required>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="input_fields_container">
                        <div class="form-group col-md-3">
                            <label for="form-control-2" class="control-label">Type Name</label>
                            <input type="text" class="form-control" id="type_name" name="type_name[]" placeholder="Type Name" data-error="Please enter Type name." required>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="form-control-2" class="control-label">Type Price</label>
                            <input type="number" class="form-control" id="type_price" name="type_price[]" placeholder="Product Price" data-error="Please enter Type price." onkeypress="return isNumberKey(event)">
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group col-md-2">
                            <span><button type="button" class="btn btn-success add_more_button"> <i class="zmdi zmdi-plus-circle zmdi-hc-fw"></i></button></span>
                        </div>
                    </div>
                    <div class="clear_fix"></div>
                    
                    <button type="submit" name="submit" value="Submit"  class="btn btn-primary btn-block">Submit</button>
                </form>
              </div>
            </div>
            <hr>
          </div>
        </div>
      </div>
      <?php include_once 'admin_includes/footer.php'; ?>
   <script src="js/tables-datatables.min.js"></script>
   <script src="js/multi_image_upload.js"></script>
   <link rel="stylesheet" type="text/css" href="css/multi_image_upload.css">
   
   <!-- Below script for Add More -->
   <script>
        $(document).ready(function() {
        var max_fields_limit      = 10; //set limit for maximum input fields
        var x = 1; //initialize counter for text box
        $('.add_more_button').click(function(e){ //click event on add more fields button having class add_more_button
            e.preventDefault();
            if(x < max_fields_limit){ //check conditions
                x++; //counter increment
                $('.input_fields_container').append('<div><div class="form-group col-md-3"><label for="form-control-2" class="control-label">Type Name</label><input type="text" class="form-control" id="type_name" name="type_name[]" placeholder="Type Name" data-error="Please enter Type name." required><div class="help-block with-errors"></div></div><div class="form-group col-md-3"><label for="form-control-2" class="control-label">Type Price</label><input type="number" class="form-control" id="type_price" name="type_price[]" placeholder="Type Price" data-error="Please enter Type price." required onkeypress="return isNumberKey(event)"><div class="help-block with-errors"></div></div><a href="#" class="remove_field btn btn-warning" style="margin-left:15px"><i class="zmdi zmdi-minus-circle zmdi-hc-fw"></i></a></div>'); //add input field
            }
        });  
        $('.input_fields_container').on("click",".remove_field", function(e){ //user click on remove text links
            e.preventDefault(); $(this).parent('div').remove(); x--;
        })
    });
    </script>