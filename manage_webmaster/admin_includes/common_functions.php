<?php
    
    function getDataFromTables($table=NULL,$status=NULL,$clause=NULL,$id=NULL,$activeStatus=NULL,$activeTop=NULL) {

        global $conn;
        if($table!='' && $table!=NULL && $clause!='' && $clause!=NULL && $id!='' && $id!=NULL) {
            //Get All Table Data with Where Condition(4)
            $sql="select * from `$table` WHERE `$clause` = '$id' ";
        } elseif($table!='' && $table!=NULL && $status!='' && $status!=NULL) {
            //Get Active Records (3)
            $sql="select * from `$table` WHERE `status` = '$status' ORDER BY id DESC";
        } elseif($table!='' && $table!=NULL && $activeTop!='' && $activeTop!=NULL) {
            //Get All Active records top Table Data (6)
            $sql="select * from `$table` ORDER BY status, id DESC ";
        } elseif($table!='' && $table!=NULL) {
            //Get All Table Data (1)
            $sql="select * from `$table` ORDER BY status, id DESC";
        }  else {
            //Last if fail then go to this
            $sql="select * from `$table` ORDER BY status, id DESC";
        }

        $result = $conn->query($sql);
        return $result;
    }
    function getAllDataWithStatus($table,$status)  {
        global $conn;
        $sql="select * from `$table` WHERE `status` = '$status' ";
        $result = $conn->query($sql); 
        return $result;
    }
    function getIndividualDetails($id,$table,$clause)
    {
        global $conn;
        $sql="select * from `$table` where `$clause` = '$id' ";
        $result = $conn->query($sql);
        $row = $result->fetch_assoc();        
        return $row;
    }
    function checkUserAvail($table,$clause,$value){
        global $conn;
        $sql = "SELECT * FROM `$table` WHERE `$clause`= '$value' ";
        $result = $conn->query($sql);
        return $result->num_rows;
    }
    /*Common function with where out where get all data from query */
    function getAllData($table)
    {
        global $conn;
        $sql="select * from `$table` ";
        $result = $conn->query($sql);         
        return $result;
    }
    
    /*Common function with where out where get all data from query */
    function getAllDataWithActiveRecent($table)  {
        global $conn;
        $sql="select * from `$table` ORDER BY status, id DESC ";
        $result = $conn->query($sql); 
        return $result;
    }

     /*Common function with where and check active status for get all records*/
    function getAllDataCheckActiveRecords($table,$status)
    {
        global $conn;
        $sql="select * from `$table` WHERE `status` = '$status' ORDER BY id DESC  ";
        $result = $conn->query($sql);         
        return $result;
    }

    /*Common function with where clause */
    function getAllDataWhere($table,$clause,$id)
    {
        global $conn;
      $sql="select * from `$table` WHERE `$clause` = '$id' "; 
        $result = $conn->query($sql);        
        return $result;
    }

    /* Common function for get count for rows */
     function getRowsCount($table)  {
        global $conn;
        $sql="select * from `$table` ";
        $result = $conn->query($sql);
        $noRows = $result->num_rows;
        return $noRows;
    }

    /* encrypt and decrypt password */
     function encryptPassword($pwd) {
        $key = "123";
        $admin_pwd = bin2hex(openssl_encrypt($pwd,'AES-128-CBC', $key));
        return $admin_pwd;
    }

    function decryptPassword($admin_password) {
        $key = "123";
        $admin_pwd = openssl_decrypt(hex2bin($admin_password),'AES-128-CBC',$key);
        return $admin_pwd;
    }

    function sendMobileOTP($user_otp,$user_mobile) {
        global $conn;
        $username = "vivek";
        $password = "12345";
        $numbers = "$user_mobile"; // mobile number
        $sender = urlencode('FDDOSE'); // assigned Sender_ID
        $message = urlencode('OTP from Chickychar is '.$user_otp.' . Do not share it with any one.'); // Message text required to deliver on mobile number
        //$data = "userid="."$username"."&password="."$password"."&mobno="."$numbers"."&msg="."$message"."&senderid="."$sender"&route=4";
        $data = "http://msgalert.in/api?userid=vivek&password=12345&mobno=".$numbers."&msg=".$message."&senderid=FDDOSE&route=4";
        $ch = curl_init();
        //curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
curl_setopt($ch,CURLOPT_URL,$data);
curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        $response = curl_exec($ch); 


    }

    function sendMobileOrderPlaced($orderId,$user_mobile,$orderTotal) {
        global $conn;
        $username = "vivek";
        $password = "12345";
        $numbers = "$user_mobile"; // mobile number
        $sender = urlencode('FDDOSE'); // assigned Sender_ID
        $message = urlencode('Thank You for Ordering FOODDOSE. Your Order Number is: '.$orderId.' Order Total: '.$orderTotal.' '); // Message text required to deliver on mobile number
        //$data = "userid="."$username"."&password="."$password"."&mobno="."$numbers"."&msg="."$message"."&senderid="."$sender"&route=4";
        $data = "http://msgalert.in/api?userid=vivek&password=12345&mobno=".$numbers."&msg=".$message."&senderid=FDDOSE&route=4";
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,$data);
        $response = curl_exec($ch); 
    }

function sendMobileOrderCancelled($orderId,$user_mobile) {
        global $conn;
        $username = "vivek";
        $password = "12345";
        $numbers = "$user_mobile"; // mobile number
        $sender = urlencode('FDDOSE'); // assigned Sender_ID
        $message = urlencode('Your order is cancelled. Apologies for any inconvenience. Your Order Number is: '.$orderId.' '); // Message text required to deliver on mobile number
        //$data = "userid="."$username"."&password="."$password"."&mobno="."$numbers"."&msg="."$message"."&senderid="."$sender"&route=4";
        $data = "http://msgalert.in/api?userid=vivek&password=12345&mobno=".$numbers."&msg=".$message."&senderid=FDDOSE&route=4";
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,$data);
        $response = curl_exec($ch); 
    }

    function getImageUnlink($val,$table,$clause,$id,$target_dir) {
        global $conn;
        $getBanner = "SELECT $val FROM $table WHERE $clause='$id' ";
        $getRes = $conn->query($getBanner);
        $row = $getRes->fetch_assoc();
        $img = $row[$val];
        $path = $target_dir.$img.'';
        chown($path, 666);
        if (unlink($path)) {
            return 1;
        } else {
            return 0;
        }
    }
    function getColumnData($sql){
       global $conn;        
       $result = $conn->query($sql);
       $row = $result->fetch_assoc();        
       return $row;
   }
    function getCategory($id){
        $data = "select category_name from `categories` where id='$id'";
        /*echo $data; die;*/
        $dataVal = getColumnData($data);
        return $dataVal['category_name'];
    }
    function getSubCategory($id){
        $data = "select sub_category_name from `sub_categories` where id='$id'";
        /*echo $data; die;*/
        $dataVal = getColumnData($data);
        return $dataVal['sub_category_name'];
    }
    function getSingleColumnName($value,$column,$expColumn,$table){
        global $conn;
        $sql="select $expColumn from `$table` WHERE $column = '$value'";
        $result = $conn->query($sql); 
        $row = $result->fetch_assoc();
        if($row){
            return $row[$expColumn];
        }
        else{
            return "" ;
        }
    }
     function getSingleColumnValue($table,$expect_column,$column,$id){
         $data = "select $expect_column  from $table where $column='$id'";
        /*echo $data; die;*/
        $dataVal = getColumnData($data);
        return $dataVal[$expect_column];
    }


?>
