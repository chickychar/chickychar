<?php include_once 'admin_includes/main_header.php'; ?>	
<?php  
if (!isset($_POST['submit']))  {
            echo "";
} else  {
    //print_r($_POST);exit;
    //Save data into database
    $product_code = rand(1234,9876);
    $cat_id = $_POST['cat_id'];
    $subcat_id = $_POST['subcat_id'];
    $product_name = $_POST['product_name'];
    $price_type_id = $_POST['price_type_id'];
    $regular_price = $_POST['regular_price'];
    $large_price = $_POST['large_price'];
    $minutes = $_POST['minutes'];

    if($price_type_id == 1) {
        $regular_price1 = $regular_price;
        $large_price1 =0;
        $minutes1 =0;
    }
    if($price_type_id == 2){
        $regular_price1 = $regular_price;
        $large_price1 = $large_price;
        $minutes1 =0;
    }

    if($price_type_id == 3){
        $regular_price1 = $regular_price;
        $minutes1 =$minutes;
        $large_price1 =0;
    }
    $fileToUpload = $_FILES["fileToUpload"]["name"];
    $created_at = date("Y-m-d h:i:s");

    if($fileToUpload!='') {

        $target_dir = "../uploads/catelog_images/";
        $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

        if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
            $sql = "INSERT INTO catering (`product_code`,`cat_id`,`subcat_id`,`product_name`,`price_type_id`,`regular_price`,`large_price`,`minutes`,`image`) VALUES ('$product_code','$cat_id','$subcat_id','$product_name','$price_type_id','$regular_price1','$large_price1','$minutes1','$fileToUpload')";
            //echo $sql; die;
            $result = $conn->query($sql);
            if($result == 1){
                echo "<script type='text/javascript'>window.location='catering.php?msg=success'</script>";
            } else {
                echo "<script type='text/javascript'>window.location='catering.php?msg=fail'</script>";
            }
        } else {
            echo "Sorry, there was an error uploading your file.";
        }
    }      
    
}
?>
      <div class="site-content">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="m-y-0">Add Catelog</h3>
          </div>
          <div class="panel-body">
            <div class="row">
              <?php $getCategories = getAllDataWithStatus('categories','0');?>
                <div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                    <form data-toggle="validator" method="post" enctype="multipart/form-data">
                    
                    <div class="form-group">
                        <label for="form-control-3" class="control-label">Select Category</label>
                        <select id="form-control-3" name="cat_id" class="custom-select cat_id" data-error="This field is required." required >
                        <option value="">Select Category</option>
                        <?php while($row = $getCategories->fetch_assoc()) {  ?>
                            <option value="<?php echo $row['id']; ?>"><?php echo $row['category_name']; ?></option>
                        <?php } ?>
                    </select>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label for="form-control-3" class="control-label">Select Sub Category</label>
                        <select id="form-control-3" name="subcat_id" class="custom-select sub_cat_id" data-error="This field is required." required >
                        <option value="">Select Sub Category</option>
                    </select>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label for="form-control-2" class="control-label">Product Name</label>
                        <input type="text" class="form-control" id="product_name" name="product_name" placeholder="Product Name" data-error="Please Enter Product name." required>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label for="form-control-3" class="control-label">Product Price Type</label>
                        <select id="price_type_id" name="price_type_id" class="custom-select" data-error="This field is required." required>
                                <option >Product Price Type</option>
                                <option value="1">Normal</option>
                                <option value="2">REG / LEG</option>
                                <option value="3">Min</option>
                        </select>
                        <div class="help-block with-errors"></div>
                    </div>

                    <div class="form-group" id="actual_price_div">
                        <label for="form-control-2" class="control-label">Regular Price</label>
                        <input type="text" class="form-control" id="regular_price" name="regular_price" placeholder="Regular Price" data-error="Please Enter Regular Price." >
                        <div class="help-block with-errors"></div>
                    </div>

                    <div id="info_div">
                    <div class="form-group">
                        <label for="form-control-2" class="control-label">Larage Price</label>
                        <input type="text" class="form-control" id="large_price" name="large_price" placeholder="Larage Price" data-error="Please Enter Larage Price." >
                        <div class="help-block with-errors"></div>
                    </div>
                    </div>

                    <div id="min_div">
                    <div class="form-group">
                        <label for="form-control-2" class="control-label">Minutes</label>
                        <input type="text" class="form-control" id="min_id" name="minutes" placeholder="Minutes" data-error="Please Enter Minutes." >
                        <div class="help-block with-errors"></div>
                    </div>
                    </div>
                    <div class="form-group">
                        <label for="form-control-4" class="control-label">Image</label>
                        <img id="output" height="100" width="100"/>
                        <label class="btn btn-default file-upload-btn">
                        Choose file...
                            <input id="form-control-22" class="file-upload-input" type="file" accept="image/*" name="fileToUpload" id="fileToUpload"  onchange="loadFile(event)"  multiple="multiple" required >
                        </label>
                    </div>
                    <button type="submit" name="submit" value="Submit"  class="btn btn-primary btn-block">Submit</button>
                    </form>
                </div>
            </div>
            <hr>
          </div>
        </div>
      </div>
      <?php include_once 'admin_includes/footer.php'; ?>
   <script src="js/tables-datatables.min.js"></script>
   <script src="js/multi_image_upload.js"></script>
   <link rel="stylesheet" type="text/css" href="css/multi_image_upload.css">
   
   <!-- Below script for ck editor -->
<script src="//cdn.ckeditor.com/4.7.0/full/ckeditor.js"></script>
<script>
    /*CKEDITOR.replace( 'product_info' );*/
</script>
<script type="text/javascript">

</script>
<script>
$("body").on("change",".cat_id", function (e) {
    var id =$(this).val();
    $.ajax({
    type: "POST",
    url: "get_sub_categories.php",
    data:'category_id='+id,
    success: function(data){
        $(".sub_cat_id").html(data);
    }
    });
});
</script>
<script src="//cdn.ckeditor.com/4.7.0/full/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'description' ); 
</script>
<style type="text/css">
    .cke_top, .cke_contents, .cke_bottom {
        border: 1px solid #333;
    }
</style>


<script type="text/javascript">
        $(document).ready(function () {
          $('#actual_price_div').hide();
          $('#info_div').hide();
          $('#min_div').hide();
          $('#price_type_id').change(function() {
             if($(this).val() == 1) {
                $('#actual_price_div').show();
                $('#info_div').hide();
                $('#min_div').hide();
                $("#regular_price").attr("required", "true");
             }else if($(this).val() == 2){
                $('#actual_price_div').show();
                $('#info_div').show();
                $('#min_div').hide();
                $("#regular_price,#large_price").removeAttr('required');
             } 
             else if($(this).val() == 3){
                $('#info_div').hide();
                $('#actual_price_div').show();
                $('#min_div').show();
                $("#regular_price,#min_id").removeAttr('required');
             }   
          });
        });  
    </script>