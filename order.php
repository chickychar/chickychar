<!DOCTYPE html>
<html lang="zxx" class="no-js">

<head>
    <?php include_once'metahead.php';?>
</head>

<body>
    <header id="header">
      <?php include_once'header.php';?>
    </header>

    <!-- start banner Area -->
    <section class="about-banner relative">
        <div class="overlay overlay-bg"></div>
        <div class="container">
            <div class="row d-flex align-items-center justify-content-center">
                <div class="about-content col-lg-12">
                    <h1 class="text-white">
                        Menus
                    </h1>
                    <p class="text-white link-nav"><a href="index.html">Home </a> <span class="lnr lnr-arrow-right"></span> <a href="menu.html"> Menus</a></p>
                </div>
            </div>
        </div>
    </section>
    <!-- End banner Area -->

    <section class="section section-sm section-first bg-default text-md-left">
        <div class="container">
            <h1 class="mt-30 mb-30">Orders</h1>
            <div class="row mb-2 b-we">

                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 orders-list-page">
                            <div class="orders-list-pagee">
                                <!--  <div class="card flex-md-row mb-4  h-md-250">

                                <img class="img-fluid order-img" alt="Burger" src="img/menu/chik.png">
                                <div class="card-body d-flex flex-column align-items-start">
                                    <h4 class="mb-0">
                                        <a class="text-red" href="#">Burger King</a>
                                    </h4>
                                    <div class="mb-1 text-muted">Madhapur Hyderabad</div>
                                    

                                </div>
                            </div>-->
                                <div class="media pt-3">
                                    <img src="img/menu/chik.png" alt="John Doe" class="img-fluid mr-3 mt-3 order-img">
                                    <div class="media-body pt-3">
                                        <h4 class="mb-0">
                                            <a class="" href="#">Burger King</a>
                                        </h4>
                                        <p>Madhapur Hyderabad</p>
                                    </div>
                                </div>
                                <div class="chip mb-20">
                                    <img src="img/menu/chik.png" alt="Person" width="96" height="96">
                                    Bread Omlete
                                    <span class="closebtn" onclick="this.parentElement.style.display='none'">×</span>
                                </div>
                                <div class="chip mb-20">
                                    <img src="img/menu/chik.png" alt="Person" width="96" height="96">
                                    Bread Omlete
                                    <span class="closebtn" onclick="this.parentElement.style.display='none'">×</span>
                                </div>
                                <div class="chip mb-20">
                                    <img src="img/menu/chik.png" alt="Person" width="96" height="96">
                                    Bread Omlete
                                    <span class="closebtn" onclick="this.parentElement.style.display='none'">×</span>
                                </div>
                                <div class="chip mb-20">
                                    <img src="img/menu/chik.png" alt="Person" width="96" height="96">
                                    Bread Omlete
                                    <span class="closebtn" onclick="this.parentElement.style.display='none'">×</span>
                                </div>
                                 <div class="row">
                                <a href="order-confirm.php"><button class="cart-btn primary mt-2 mb-20">Order</button></a>
                            </div>
                            </div>
                           
                             <div class="orders-list-pagee">
                                <!--  <div class="card flex-md-row mb-4  h-md-250">

                                <img class="img-fluid order-img" alt="Burger" src="img/menu/chik.png">
                                <div class="card-body d-flex flex-column align-items-start">
                                    <h4 class="mb-0">
                                        <a class="text-red" href="#">Burger King</a>
                                    </h4>
                                    <div class="mb-1 text-muted">Madhapur Hyderabad</div>
                                    

                                </div>
                            </div>-->
                                <div class="media pt-3">
                                    <img src="img/menu/chik.png" alt="John Doe" class="img-fluid mr-3 mt-3 order-img">
                                    <div class="media-body pt-3">
                                        <h4 class="mb-0">
                                            <a class="" href="#">Burger King</a>
                                        </h4>
                                        <p>Madhapur Hyderabad</p>
                                    </div>
                                </div>
                                <div class="chip mb-20">
                                    <img src="img/menu/chik.png" alt="Person" width="96" height="96">
                                    Bread Omlete
                                    <span class="closebtn" onclick="this.parentElement.style.display='none'">×</span>
                                </div>
                                <div class="chip mb-20">
                                    <img src="img/menu/chik.png" alt="Person" width="96" height="96">
                                    Bread Omlete
                                    <span class="closebtn" onclick="this.parentElement.style.display='none'">×</span>
                                </div>
                                <div class="chip mb-20">
                                    <img src="img/menu/chik.png" alt="Person" width="96" height="96">
                                    Bread Omlete
                                    <span class="closebtn" onclick="this.parentElement.style.display='none'">×</span>
                                </div>
                                <div class="chip mb-20">
                                    <img src="img/menu/chik.png" alt="Person" width="96" height="96">
                                    Bread Omlete
                                    <span class="closebtn" onclick="this.parentElement.style.display='none'">×</span>
                                </div>
                                  <div class="row">
                                <a href="order-confirm.php"><button class="cart-btn primary mt-2 mb-20">Order</button></a>
                            </div>
                            </div>
                           

                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">

                            <div class="order-details">
                                <div class="d-flex mb-3">
                                    <div class="p-2 mr-auto">Items</div>


                                </div>
                                <div class="border-bottom bordd"></div>
                                <div class="d-flex mb-3">
                                    <div class="p-2 mr-auto">Idly</div>

                                    <div class="p-2">
                                        <span>x1</span>
                                    </div>
                                </div>
                                <div class="border-bottom bordd"></div>
                                <div class="d-flex mb-3">
                                    <div class="p-2 mr-auto">Chicken Burger</div>

                                    <div class="p-2">
                                        <span>x1</span>
                                    </div>
                                </div>
                                <div class="border-bottom bordd"></div>
                                <div class="d-flex mb-3">
                                    <div class="p-2 mr-auto">Order</div>

                                    <div class="p-2">
                                        <span>BKG 22/8/2018</span>
                                    </div>
                                </div>
                                <div class="border-bottom bordd"></div>
                                <div class="d-flex mb-3">
                                    <div class="p-2 mr-auto">Delivery Address</div>

                                    <div class="p-2">
                                        <span>VRK Sunshine</span>
                                        <span>Madhapur,hyderabad</span>
                                    </div>
                                </div>
                                <div class="border-bottom bordd"></div>
                                <div class="d-flex mb-3">
                                    <div class="p-2 mr-auto">Estimated Arrival</div>

                                    <div class="p-2">
                                        <span>30min</span>
                                    </div>
                                </div>
                                <div class="border-bottom bordd"></div>
                                <div class="d-flex mb-3">
                                    <div class="p-2 mr-auto">To Pay</div>

                                    <div class="p-2">
                                        <span>₹ 45</span>
                                    </div>
                                </div>

                            </div>


                        </div>

                    </div>




                </div>


            </div>

        </div>
    </section>
    <footer class="footer-area">
        <?php include_once'footer.php';?>
    </footer>

</body>

</html>
