<!DOCTYPE html>
<html lang="zxx" class="no-js">

 <head>
    <?php include_once'metahead.php';?>
</head>

<body>
    <header id="header">
      <?php include_once'header.php';?>
    </header>

    <!-- start banner Area -->
    <section class="about-banner relative">
        <div class="overlay overlay-bg"></div>
        <div class="container">
            <div class="row d-flex align-items-center justify-content-center">
                <div class="about-content col-lg-12">
                    <h1 class="text-white">
                        Menus
                    </h1>
                    <p class="text-white link-nav"><a href="index.html">Home </a> <span class="lnr lnr-arrow-right"></span> <a href="menu.html"> Menus</a></p>
                </div>
            </div>
        </div>
    </section>
    <!-- End banner Area -->

    <section class="section section-sm section-first bg-default text-md-left">
        <div class="container">
            <div class="row row-50 pt-5 justify-content-center mb-4">
                <div class="col-md-10 col-lg-6">
                    <h3 class="font-weight-medium">Change Password</h3>
                    <form class="form-checkout b-we mt-3">
                        <div class="row row-30">

                            <div class="col-12">
                                <div class="form-wrap">
                                    <input class="form-input" id="email" type="email" name="name" data-constraints="@Required" placeholder="Mobile">

                                </div>
                            </div>
                         

                            <div class="col-12 pt-3">
                                <button class="login-btn primary mb-3 text-center">Change Password</button>
                                
                            </div>


                        </div>


                    </form>
                </div>
              
            </div>
        </div>
    </section>
   <footer class="footer-area">
        <?php include_once'footer.php';?>
    </footer>


    <script>
        $(document).ready(function() {
            // Add scrollspy to <body>
            $('.category-menu').scrollspy({
                target: ".category-menu",
                offset: 50
            });

            // Add smooth scrolling on all links inside the navbar
            $("#myScrollspy a").on('click', function(event) {
                // Make sure this.hash has a value before overriding default behavior
                if (this.hash !== "") {
                    // Prevent default anchor click behavior
                    event.preventDefault();

                    // Store hash
                    var hash = this.hash;

                    // Using jQuery's animate() method to add smooth page scroll
                    // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
                    $('html, body').animate({
                        scrollTop: $(hash).offset().top
                    }, 900, function() {

                        // Add hash (#) to URL when done scrolling (default click behavior)
                        window.location.hash = hash;
                    });
                } // End if
            });
        });

    </script>


</body>

</html>
