<!DOCTYPE html>
<html lang="zxx" class="no-js">

<head>
    <?php include_once'metahead.php';?>
</head>

<body>
    <header id="header">
        <?php include_once'header.php';?>
    </header>

    <!-- start banner Area -->
    <section class="about-banner relative">
        <div class="overlay overlay-bg"></div>
        <div class="container">
            <div class="row d-flex align-items-center justify-content-center">
                <div class="about-content col-lg-12">
                    <h1 class="text-white">
                        Menus
                    </h1>
                    <p class="text-white link-nav"><a href="index.html">Home </a> <span class="lnr lnr-arrow-right"></span> <a href="menu.html"> Menus</a></p>
                </div>
            </div>
        </div>
    </section>
    <!-- End banner Area -->

    <section class="section section-sm section-first bg-default text-md-left">

        <div class="container ">
            <h1 class="mt-30 mb-30">Cart</h1>
            <div class="table-responsive cart-w rounded">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Product</th>
                            <th>Quantity</th>
                            <th>Availability</th>
                            <th>Price</th>
                            <th>Total</th>
                            <th>Remove</th>

                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><img class="img-fluid rounded order-img" src="img/menu/chik.png"><span class="add-cart">Burger</span></td>
                            <td>2</td>
                            <td>Available now</td>
                            <td>20$</td>
                            <td>20$</td>
                            <td><i class="fa fa-times" aria-hidden="true"></i></td>

                        </tr>
                        <tr>
                            <td><img class="img-fluid rounded order-img" src="img/menu/chik.png"><span class="add-cart">Burger</span></td>
                            <td>2</td>
                            <td>Available now</td>
                            <td>20$</td>
                            <td>20$</td>
                            <td><i class="fa fa-times" aria-hidden="true"></i></td>

                        </tr>
                        <tr>
                            <td><img class="img-fluid rounded order-img" src="img/menu/chik.png"><span class="add-cart">Burger</span></td>
                            <td>2</td>
                            <td>Available now</td>
                            <td>20$</td>
                            <td>20$</td>
                            <td><i class="fa fa-times" aria-hidden="true"></i></td>

                        </tr>
                    </tbody>
                </table>
            </div>


        </div>
        <div class="container">
        <div class="row mt-3 mb-3">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
            <div class=" checkout-cart-total cart-w rounded">

                                    <h2 class=" ">Cart Total</h2>
                                   
                                   
                                    <ul>

                                        <li>Item total<span>$50.00</span></li>
                                        <li>Restaurant Charges<span>$4.00</span></li>

                                        <li class="border-bottom"></li>
                                        <li>Delivery Fee<span>$50.00</span></li>
                                        <li class="border-bottom"></li>
                                        <li>TO PAY<span>$50.00</span></li>

                                          <a class="cart-btn primary mt-2" href="checkoutlogin.php">Proceed To Checkout</a>

                                    </ul>


                                </div>
                </div>
            </div>
        </div>
    </section>
    <footer class="footer-area">
        <?php include_once'footer.php';?>
    </footer>


    <script>
        $(document).ready(function() {
            // Add scrollspy to <body>
            $('.category-menu').scrollspy({
                target: ".category-menu",
                offset: 50
            });

            // Add smooth scrolling on all links inside the navbar
            $("#myScrollspy a").on('click', function(event) {
                // Make sure this.hash has a value before overriding default behavior
                if (this.hash !== "") {
                    // Prevent default anchor click behavior
                    event.preventDefault();

                    // Store hash
                    var hash = this.hash;

                    // Using jQuery's animate() method to add smooth page scroll
                    // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
                    $('html, body').animate({
                        scrollTop: $(hash).offset().top
                    }, 900, function() {

                        // Add hash (#) to URL when done scrolling (default click behavior)
                        window.location.hash = hash;
                    });
                } // End if
            });
        });

    </script>


</body>

</html>
