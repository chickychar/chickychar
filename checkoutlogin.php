<!DOCTYPE html>
<html lang="zxx" class="no-js">

<head>
    <?php include_once'metahead.php';?>
</head>

<body>
    <header id="header">
        <?php include_once'header.php';?>
    </header>

    <!-- start banner Area -->
    <section class="about-banner relative">
        <div class="overlay overlay-bg"></div>
        <div class="container">
            <div class="row d-flex align-items-center justify-content-center">
                <div class="about-content col-lg-12">
                    <h1 class="text-white">
                        Menus
                    </h1>
                    <p class="text-white link-nav"><a href="index.html">Home </a> <span class="lnr lnr-arrow-right"></span> <a href="menu.html"> Menus</a></p>
                </div>
            </div>
        </div>
    </section>
    <!-- End banner Area -->

    <section class="section section-sm section-first bg-default text-md-left">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 mb-sm-35 mb-xs-35">

                    <!--=======  Grid list view  =======-->
                    <div class="container-fluid">
                        <div class="row pt-5">
                            <div class="col-lg-7 col-md-7 col-sm-12 col-12">
                                <div class="row b-we p-3">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-12 checkout-asap">
                                        <h2>Account</h2>
                                        <p>To place your order now, log in to your existing account or sign up.</p>
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-6">
                                                <button class="cart-btn primary mt-2 mb-20" id="loginc">LOGIN</button>



                                            </div>

                                            <div class="col-lg-6 col-md-6 col-sm-6 col-6">
                                                <button class="cart-btn primary mt-2 mb-20" id="signc">SIGNUP</button>

                                            </div>

                                        </div>
                                        <div class="row" id="cl-first">
                                            <div class="col-xl-12 col-lg-12 col-sm-12">

                                                <div class="bg-checkout">
                                                    <!-- Login Form s-->
                                                    <div id="firstc">
                                                        <form action="#" class="">

                                                            <div class="col-md-12 col-12 mb-20 form-group has-float-label">
                                                                <input class="form-input form-l" id="mobile" type="text" placeholder="Mobile">
                                                            </div>
                                                            <div class="col-md-12 col-12 mb-20 form-group has-float-label">
                                                                <input class="form-input form-l" id="password" type="password" placeholder="password">
                                                            </div>
                                                            <a href="checkout-login.php">
                                                                <div class="col-md-12 col-12">
                                                                    <button class="cart-btn primary mt-2 mb-20">Login</button>
                                                                </div>
                                                            </a>
                                                        </form>


                                                    </div>
                                                    <div id="secondc">

                                                        <form action="#" class="">
                                                            <div class="col-md-12 col-12 mb-20 form-group has-float-label">
                                                                <input class="form-input form-l" id="name" type="text" placeholder="Name">
                                                            </div>
                                                            <div class="col-md-12 col-12 mb-20 form-group has-float-label">
                                                                <input class="form-input form-l" id="email" type="email" placeholder="Email">
                                                            </div>
                                                            <div class="col-md-12 col-12 mb-20 form-group has-float-label">
                                                                <input class="form-input form-l" id="mobile" type="text" placeholder="Mobile">
                                                            </div>
                                                            <div class="col-md-12 col-12 mb-20 form-group has-float-label">
                                                                <input class="form-input form-l" id="password" type="password" placeholder="password">
                                                            </div>
                                                            <div class="col-md-12 col-12 mb-20 form-group has-float-label">
                                                                <input class="form-input form-l" id="cnfrmpassword" type="password" placeholder="Confirm password">
                                                            </div>
                                                            <a href="#"><button class="cart-btn primary mt-2 mb-20">SignUp</button></a>

                                                        </form>

                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="row p-3 rounded">
                                    <div class="col-lg-7 col-md-7 col-sm-12 col-12 checkout-asap">
                                        <h2>Logged In</h2>
                                        <p>Lancius | 8989898989</p>


                                    </div>

                                </div>

                                    </div>
                                    <div class="col-5 check-img">
                                        <img src="assets/images/categories/category3.png" class="img-fluid" alt="">
                                    </div>
                                </div>
                                  <div class="row mt-30 shadow-sm p-3 b-we rounded mb-3">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-12 ">

                                        <h2>Select Delivery Address</h2>
                                    </div>
                                    <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12 p-3 ">
                                        <address class="address border p-3 rounded small">
                                            <h4><span class="icon_pin_alt"></span>Add New Address</h4>
                                            <p>Shop 1, 145 McEvoy Street Alexandria NSW 2015

                                                02 9318 2000</p>
                                        </address>

                                    </div>
                                    <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12 p-3 ">
                                        <address class="border p-3 rounded small">
                                            <h4><span class="icon_pin_alt"></span>Add New Address</h4>
                                            <p class="text-center" style="font-size:30px;">+</p>
                                        </address>

                                    </div>
                                </div>
                                
                            </div>
                            <div class="col-lg-5 col-md-5 col-sm-12 col-12">
                                <div class=" checkout-cart-total cart-w rounded">

                                    <h2 class=" ">Cart</h2>
                                    <div class="row mb-20">
                                        <div class="col-lg-4 col-md-8 col-sm-6 col-6">

                                            <img class="img-fluid" src="img/menu/chik.png">
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-6 col-6">
                                            <p>Burger King</p><small>Madhapur,Hyderabad</small>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6 col-4">Palak Paneer</div>
                                        <div class="col-lg-3 col-4"><button type="button" class="btn btn-light add-btn">- 1 +</button></div>
                                        <div class="col-lg-3 col-4"><span>$25.00</span></div>
                                    </div>
                                    <div class="container mt-20 mb-20">
                                        <input class="form-control form-c" placeholder="Any suggestions? we will pass it on">
                                    </div>
                                    <ul>

                                        <li>Item total<span>$50.00</span></li>
                                        <li>Restaurant Charges<span>$4.00</span></li>

                                        <li class="border-bottom"></li>
                                        <li>Delivery Fee<span>$50.00</span></li>
                                        <li class="border-bottom"></li>
                                        <li>TO PAY<span>$50.00</span></li>



                                    </ul>


                                </div>
                                 <div class="cart-w rounded my-account-section mt-5 ">

                                            <div class="container payment">
                                                <div class="row">
                                                    <div class="col-12">

                                                        <div class="row">

                                                            <!-- My Account Tab Menu Start -->
                                                            <div class="col-lg-12 col-12">
                                                                <h2 class="pt-3">Payment</h2>
                                                               <div class="" id="myaccountContent">
                                                                    <!-- Single Tab Content Start -->
                                                                    <div class="" id="dashboad">


                                                                        <div class="myaccount-content">
                                                                            <div class="row">
                                                                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-3 col-3">
                                                                                    <p>We Accept</p>
                                                                                </div>
                                                                                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2">
                                                                                    <img src="img/visa.png">
                                                                                </div>
                                                                                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2">
                                                                                    <img src="img/m.png">
                                                                                </div>
                                                                                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2">
                                                                                    <img src="img/m2.png">
                                                                                </div>
                                                                                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2">
                                                                                    <img src="img//m3.png">
                                                                                </div>
                                                                            </div>
                                                                            <a href="orderconfirm.php"><button class="cart-btn primary mt-2">Pay</button>
                                                                            </a>


                                                                        </div>

                                                                    </div>


                                                                </div>
                                                            </div>
                                                           

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                            </div>
                        </div>

                        <!--=======  End of Grid list view  =======-->



                    </div>
                </div>
            </div>
        </div>
    </section>
    <footer class="footer-area">
        <?php include_once'footer.php';?>
    </footer>

    
    <!--Check out loginregister form-->
    <script>
        // On Click SignUp It Will Hide Login Form and Display Registration Form
        $("#signc").click(function() {
            $("#firstc").hide("fast");
            $("#secondc").show("fast");
            $("#iic").hide("fast")

        });
        // On Click SignIn It Will Hide Registration Form and Display Login Form
        $("#loginc").click(function() {
            $("#secondc").hide("fast");
            $("#firstc").show("fast");
            $("#iic").show("fast")



        });

    </script>
  


</body>

</html>
