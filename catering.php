<!DOCTYPE html>
<html lang="zxx" class="no-js">

<head>
    <?php include_once'metahead.php';?>
    <style>

     </style>
</head>

<body>
    <header id="header">
        <?php include_once'header.php';?>
    </header>

    <!-- start banner Area -->
    <section class="about-banner relative">
        <div class="overlay overlay-bg"></div>
        <div class="container">
            <div class="row d-flex align-items-center justify-content-center">
                <div class="about-content col-lg-12">
                    <h1 class="text-white">
                        Menus
                    </h1>
                    <p class="text-white link-nav"><a href="index.html">Home </a> <span class="lnr lnr-arrow-right"></span> <a href="menu.php"> Menus</a></p>
                </div>
            </div>
        </div>
    </section>
    <!-- End banner Area -->

    <!-- Start menu-area Area -->
    <div class="clearfix"></div>
    <section class="menu-area section-gapp" id="menu">
       
      
        <div class="container">

            <div class="row">


                <div class="col-lg-8 col-md-12 ">
                    <div class="food-menu">
                        <h2 class="price mt-5 mb-2">Break Fast</h2>
                        <div class="row">
                            <?php for($s=0; $s<6; $s++) {?>
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">

                                <div class="single-menu ">
                                    <div class="image">
                                        <img class="img-fluid" src="img/menu/1.jpg">
                                    </div>
                                    <div class="title-wrap d-flex justify-content-between pt-3">
                                        <h5>Sourdough Toast
                                        </h5>
                                        <h5 class="price">$5.90</h5>
                                    </div>
                                    <p>
                                        With wild berry jam / marmalade / peanut butter / vegemite
                                    </p>
                                    <div class="row">
                                        <div class="col-md-6 col-6">
                                        <form action="/action_page.php">
                                       <div class="form-group">
                                       <select class="form-control cat-form" id="sel1" name="sellist1">
                                         <option>Regular</option>
                                         <option>Large</option>
                                   
                                             </select>
                                           </div>
                                          </form>
                                        </div>
                                        <div class="col-md-6 col-6">
                                         <button class="catering primary pull-right">Add To Cart</button>
                                            </div>
                                    </div>
                                    
                                </div>
                                
                            </div>
                            <?php } ?>

                        </div>


                    </div>

                    <div class="food-menu">
                        <h2 class="price mt-5 mb-2">Gourmet Burgers</h2>
                          <div class="row">
                            <?php for($s=0; $s<6; $s++) {?>
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">

                                <div class="single-menu ">
                                    <div class="image">
                                        <img class="img-fluid" src="img/menu/1.jpg">
                                    </div>
                                    <div class="title-wrap d-flex justify-content-between pt-3">
                                        <h5>Sourdough Toast
                                        </h5>
                                        <h5 class="price">$5.90</h5>
                                    </div>
                                    <p>
                                        With wild berry jam / marmalade / peanut butter / vegemite
                                    </p>
                                    <div class="row">
                                        <div class="col-md-6">
                                        <form action="/action_page.php">
                                       <div class="form-group">
                                       <select class="form-control cat-form" id="sel1" name="sellist1">
                                         <option>Regular</option>
                                         <option>Large</option>
                                   
                                             </select>
                                           </div>
                                          </form>
                                        </div>
                                        <div class="col-md-6">
                                         <button class="catering primary pull-right">Add To Cart</button>
                                            </div>
                                    </div>
                                    
                                </div>
                                
                            </div>
                            <?php } ?>

                        </div>
                    </div>
                    <div class="food-menu">
                        <h2 class="price mt-5 mb-2">CharChar Chickens</h2>
                       <div class="row">
                            <?php for($s=0; $s<6; $s++) {?>
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">

                                <div class="single-menu ">
                                    <div class="image">
                                        <img class="img-fluid" src="img/menu/1.jpg">
                                    </div>
                                    <div class="title-wrap d-flex justify-content-between pt-3">
                                        <h5>Sourdough Toast
                                        </h5>
                                        <h5 class="price">$5.90</h5>
                                    </div>
                                    <p>
                                        With wild berry jam / marmalade / peanut butter / vegemite
                                    </p>
                                    <div class="row">
                                        <div class="col-md-6">
                                        <form action="/action_page.php">
                                       <div class="form-group">
                                       <select class="form-control cat-form" id="sel1" name="sellist1">
                                         <option>Regular</option>
                                         <option>Large</option>
                                   
                                             </select>
                                           </div>
                                          </form>
                                        </div>
                                        <div class="col-md-6">
                                         <button class="catering primary pull-right">Add To Cart</button>
                                            </div>
                                    </div>
                                    
                                </div>
                                
                            </div>
                            <?php } ?>

                        </div>

                    </div>
                    <div class="food-menu">
                        <h2 class="price mt-5 mb-2">Beverages</h2>
                          <div class="row">
                            <?php for($s=0; $s<6; $s++) {?>
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">

                                <div class="single-menu ">
                                    <div class="image">
                                        <img class="img-fluid" src="img/menu/1.jpg">
                                    </div>
                                    <div class="title-wrap d-flex justify-content-between pt-3">
                                        <h5>Sourdough Toast
                                        </h5>
                                        <h5 class="price">$5.90</h5>
                                    </div>
                                    <p>
                                        With wild berry jam / marmalade / peanut butter / vegemite
                                    </p>
                                    <div class="row">
                                        <div class="col-md-6">
                                        <form action="/action_page.php">
                                       <div class="form-group">
                                       <select class="form-control cat-form" id="sel1" name="sellist1">
                                         <option>Regular</option>
                                         <option>Large</option>
                                   
                                             </select>
                                           </div>
                                          </form>
                                        </div>
                                        <div class="col-md-6">
                                         <button class="catering primary pull-right">Add To Cart</button>
                                            </div>
                                    </div>
                                    
                                </div>
                                
                            </div>
                            <?php } ?>

                        </div>

                    </div>
                </div>
                <div class="col-lg-4 col-md-12 ">
                    <div class="checkout mt-5 rounded">
                      
                        <div class="row">
                            <div class="col-lg-6 col-md-5 col-4">Palak Paneer</div>
                            <div class=" col-lg-3 col-md-4 col-4"><button type="button" class="btn btn-light add-btn">- 1 +</button></div>
                            <div class="col-lg-3 col-md-3 col-4"><span>$25.00</span></div>

                        </div>
                        <ul class="mt-3">
                            <li>Subtotal<span>$50.00</span></li>


                        </ul>
                        <div class="row mt-3">
                          <a class="col-4 chekout-btn primary mb-3 text-center" href="cart.php">Checkout</a>
                            </div>
                    </div>
                </div>

            </div>

        </div>
    </section>
    <!-- End menu-area Area -->

    <!-- Start reservation Area -->
    <section class="reservation-area section-gap relative">
        <div class="overlay overlay-bg"></div>
        <div class="container">
            <div class="row justify-content-between align-items-center">
                <div class="col-lg-6 reservation-left">
                    <h1 class="text-white">If you’re organising a function, an all day workshop or even a party at home,</h1>
                    <p class="text-white pt-20">
                        We would be happy to help and offer our suggestions.
                    </p>


                </div>
                <div class="col-lg-5 reservation-right">
                    <form class="form-wrap text-center" action="catering-mail.php" method="post">
                        <input type="text" class="form-control" name="name" placeholder="Your Name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Your Name'">
                        <input type="email" class="form-control" name="email" placeholder="Your Email Address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Your Email Address'">
                        <input type="text" class="form-control" name="phone" placeholder="Phone Number" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Phone Number'">

                        <textarea class="form-control" name="additional-info" placeholder="Additional Information" required></textarea>
                        <button class="primary-btn text-uppercase mt-20">Send Enquiry</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- End reservation Area -->

    <footer class="footer-area">
        <?php include_once'footer.php';?>
    </footer>
   
</body>

</html>
